import maya.cmds as cmds
import re
from rigpackage.rigmodule.fkmodule import ctrlRig
from rigpackage.rigmodule.fkmodule import naming as N
from rigpackage.rigmodule.fkmodule import rigTools as rt
from rigpackage.rigmodule.fkmodule import core

reload(ctrlRig)
reload(N)
reload(rt)
reload(core)


from rigpackage.rigwip import addMissingInfluence as ami
reload(ami)

import pymel.core as pmc
import maya.mel as mel
import os

class Facial(object):
	# if self.facial:
	#     self.facial_module()
	def __init__(self, char = 'charEif'):
		loc_list = cmds.file(loc=1,q=1).split('/')
		loc_path = '/'.join(loc_list[:-1])
		self.char = char
		# import bsh file
		elf_bsh_file = 'elf_bsh_hero.ma'
		self.elf_bsh_path = os.path.join(loc_path,elf_bsh_file)
		

		# import facial file
		elf_fcl_file = 'elf_fcl_hero.ma'
		self.elf_fcl_path = os.path.join(loc_path,elf_fcl_file)
		cmds.file(self.elf_bsh_path,i=1)
		cmds.file(self.elf_fcl_path,i=1)

	def moduleStructure(self):	
		self.facial_module()

	def post_build(self):
		self.facial_post_module()
		cmds.delete('Fcl_Rvt_Position_Grp')


	def facial_module(self):
		# create group
		fcl_ctrl_grp = cmds.createNode('transform' , n ='Fcl_0_Ctl_Grp')
		fcl_ctrl_grp = cmds.createNode('transform' , n ='Fcl_0_Jnt_Grp')
		fcl_ctrl_grp = cmds.createNode('transform' , n ='Fcl_0_Geo_Grp')

		ami.copy_weight('{}_body_FclGeo'.format(self.char),'FaceBuffer_Fcl_ply')
		skn = mel.eval('findRelatedSkinCluster "{}";'.format('{}_body_FclGeo'.format(self.char)).format('{}_body_FclGeo'.format(self.char)))
		cmds.skinCluster(skn,ub=1,e=1)
		# do wrap
		cmds.select(cl=1)
		cmds.select(['{}_body_FclGeo'.format(self.char),'FaceBuffer_Fcl_ply'])
		#cmds.select('FaceBuffer_ply')
		#wrap = cmds.deformer('{}_body_FclGeo',type = 'wrap')[0]
		#wrap = cmds.deformer(type = 'wrap')[0]
		wrap = cmds.CreateWrap()
		cmds.setAttr('wrap1.exclusiveBind',1)
		# do blendShape
		cmds.blendShape('EyeLBuffer_bsh','{}_eye_L_0001_FclGrp'.format(self.char),foc=1,w=[0,1])
		cmds.blendShape('EyeRBuffer_bsh','{}_eye_R_0001_FclGrp'.format(self.char),foc=1,w=[0,1])
		cmds.blendShape('Mouth_0_Blnd_GeoGrp','{}_mouth_FclGrp'.format(self.char),foc=1,w=[0,1])
		cmds.blendShape('EyelashBuffer_ply','{}_eyelash_FclGeo'.format(self.char),foc=1,w=[0,1])
		cmds.blendShape('EyebrowBuffer_ply','{}_eyebrow_FclGeo'.format(self.char),foc=1,w=[0,1])

		# adjust hierarchy
		cmds.parent(['FclChain_0_Fcl_CtlGrp','BshCtrl_grp'],'Fcl_0_Ctl_Grp')
		cmds.parent('FclChain_0_Fcl_JntGrp','Fcl_0_Jnt_Grp')
		cmds.parent(['Bsh_0_Geo_Grp','Eif_0_Fcl_grp'], 'Fcl_0_Geo_Grp')
		
		# parent grp to rig system grp
		cmds.parent('Fcl_0_Ctl_Grp','Anim_Grp')
		cmds.parent(['Fcl_0_Jnt_Grp','Fcl_0_Geo_Grp'],'Still_Grp')

		# constraint ctrl
		cmds.parentConstraint('Head_0_Skin_Jnt','Fcl_0_Ctl_Grp',mo=1)

		# blendShape fclGeo to mainRig
		cmds.blendShape('{}_eye_L_0001_FclGrp'.format(self.char),'{}_eye_L_0001_grp'.format(self.char),foc=1,w=[0,1])
		cmds.blendShape('{}_eye_R_0001_FclGrp'.format(self.char),'{}_eye_R_0001_grp'.format(self.char),foc=1,w=[0,1])
		cmds.blendShape('{}_mouth_FclGrp'.format(self.char),'{}_mouth_grp'.format(self.char),foc=1,w=[0,1])
		# cmds.blendShape('{}_eyelash_FclGeo','{}_eyelash_geo',foc=1,w=[0,1])
		# cmds.blendShape('{}_eyebrow_FclGeo','{}_eyebrow_geo',foc=1,w=[0,1])
		# cmds.blendShape('{}_body_FclGeo','{}_body_geo',foc=1,w=[0,1])		
		cmds.blendShape('{}_body_FclGrp'.format(self.char),'{}_body_grp'.format(self.char),foc=1,w=[0,1])

		cmds.setAttr('Fcl_0_Geo_Grp.v',0)
		cmds.setAttr('FclChain_0_Fcl_JntGrp.v',0)


	def fix_fcl_constraint(self):
		con_list = cmds.listRelatives('FclChain_0_Fcl_JntGrp',ad=1,type='constraint')

		for con in con_list:
			par= cmds.listRelatives(con,p=1)[0]
			ctrlGrp = par.replace('_JntGrp','_CtlGrp')
			cmds.delete(con)
			for trans in 'tr':
				for ax in 'xyz':
					attr ='.'+ trans + ax
					cmds.connectAttr(ctrlGrp+ attr, par+attr)

	def create_eye_follow(self):
		rt.add_01_attr('Lf_Eye_0_Blnd_Ctl','eyeFollow')
		rt.add_01_attr('Rt_Eye_0_Blnd_Ctl','eyeFollow')

		mdv1 = cmds.createNode('multiplyDivide', n = 'Lf_CenterEye_0_mdv')
		mdv2 = cmds.createNode('multiplyDivide', n = 'Rt_CenterEye_0_mdv')


		for ax in 'xyz':
			cmds.connectAttr('Lf_Eye_0_Blnd_Ctl.eyeFollow',mdv1+'.input2{}'.format(ax.upper()))
			cmds.connectAttr('Rt_Eye_0_Blnd_Ctl.eyeFollow',mdv2+'.input2{}'.format(ax.upper()))

		cmds.connectAttr('Lf_Eye_0_Anim_Jnt.r',mdv1+'.input1')
		cmds.connectAttr('Rt_Eye_0_Anim_Jnt.r',mdv2+'.input1')
		cmds.connectAttr(mdv1+'.output','Lf_CenterEye_0_Fcl_CtlDrvGrp.r')
		cmds.connectAttr(mdv2+'.output','Rt_CenterEye_0_Fcl_CtlDrvGrp.r')

		cmds.setAttr('Lf_Eye_0_Blnd_Ctl.eyeFollow',.4)
		cmds.setAttr('Rt_Eye_0_Blnd_Ctl.eyeFollow',.4)

		cmds.parentConstraint('Head_0_Skin_Jnt','Eye_JntGrp',mo=1)

	def attach_follicle(self,mesh = 'FaceBuffer_ply',parent = ''):
		sel =cmds.ls(sl=True)
		cpom = cmds.createNode('closestPointOnMesh')
		cmds.connectAttr(mesh+'Shape.worldMesh',cpom+'.inMesh')
		fol_list = []
		for i in sel:
			ctrlGrp = i.replace('_Position','_CtlOffGrp')
			tmp = cmds.createNode('follicle')
			tmp_trans = cmds.listRelatives(tmp,p=1)[0]
			fol = cmds.rename(tmp_trans,i.replace('_Position','_Fol'))
			folShape = cmds.listRelatives(fol,s=1)[0]
			
			t = cmds.xform(i,ws=1,q=1,t=1)
			
			for idx , ax in enumerate('xyz'):
				cmds.setAttr(cpom + '.ip{}'.format(ax),t[idx])

			u = cmds.getAttr(cpom+'.parameterU')
			v = cmds.getAttr(cpom+'.parameterV')
			
			cmds.connectAttr(mesh+'.worldMesh',folShape+'.inputMesh')
			cmds.setAttr(folShape+'.parameterU',u)
			cmds.setAttr(folShape+'.parameterV',v)
			
			cmds.connectAttr(folShape+'.outTranslate' , fol+'.t')
			#mc.connectAttr(folShape+'.outRotate', fol+'.r')
			folCon = cmds.parentConstraint(fol,ctrlGrp,mo=1)[0]
			cmds.disconnectAttr(ctrlGrp +'.parentInverseMatrix' , folCon+'.constraintParentInverseMatrix')
			fol_list.append(fol)
		cmds.delete(cpom)

		if parent:
			cmds.parent(fol_list,parent)


	def create_fcl_follicle(self):
		follicle_grp = cmds.createNode('transform', n = 'Follicle_0_Fcl_Grp')
		loc_list = cmds.file(loc=1,q=1).split('/')
		loc_path = '/'.join(loc_list[:-1])
		# import rvt file
		elf_bsh_file = 'rvt_pos.ma'
		elf_bsh_path = os.path.join(loc_path,elf_bsh_file)
		cmds.file(elf_bsh_path,i=1)

		fclPos = cmds.listRelatives('FclPosition')
		browPos = cmds.listRelatives('BrowPosition')
		teethPos = cmds.listRelatives('TeethPosition')
		eyeLashPos = cmds.listRelatives('EyeLashPosition')
		cmds.select(fclPos)
		self.attach_follicle(mesh = 'FaceBuffer_ply',parent = follicle_grp)

		# re adjust hierarcy of bro ctrl
		brow_ctrlGrp = cmds.ls('*BrowOfs_*_Fcl*_CtlGrp')
		brow_ofs_grp = cmds.createNode('transform',n = 'BrowOfs_0_Fcl_CtlGrp')
		cmds.parent(brow_ctrlGrp, brow_ofs_grp)
		cmds.parent(brow_ofs_grp,'FclChain_0_Fcl_CtlGrp')


		cmds.select(browPos)
		self.attach_follicle(mesh = 'FaceBuffer_Fcl_ply',parent =follicle_grp)

		cmds.select(teethPos[0])
		self.attach_follicle('GumDBuffer_ply',parent =follicle_grp)

		cmds.select(teethPos[1])
		self.attach_follicle('GumTBuffer_ply',parent =follicle_grp)

		cmds.select(eyeLashPos)
		self.attach_follicle('FaceBuffer_Fcl_ply',parent =follicle_grp)

		cmds.parent(follicle_grp,'Still_Grp')

	def disconnect_jnt_off_grp(self):
		off_fcl_jnt_grp = cmds.ls('*Fcl_*JntOffGrp')
		for jnt in off_fcl_jnt_grp:
			#query the connection
			conT = cmds.listConnections(jnt + '.t' , source = 1 , p = 1)[0]
			conR = cmds.listConnections(jnt + '.r' , source = 1 , p = 1)[0]
			conS = cmds.listConnections(jnt + '.s' , source = 1 , p = 1)[0]
			#now break
			cmds.disconnectAttr(conT , jnt + '.t' )
			cmds.disconnectAttr(conR , jnt + '.r' )
			cmds.disconnectAttr(conS , jnt + '.s' )

	def facial_post_module(self):
		# change constraint to connection
		self.fix_fcl_constraint()

		# create eyeFollow()
		self.create_eye_follow()

		# create rivet to make controller to follow blendShape
		self.create_fcl_follicle()

		# disconnect offsetJntGrp
		self.disconnect_jnt_off_grp()