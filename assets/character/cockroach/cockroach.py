from ... import rigAssetChayaSetup

reload(rigAssetChayaSetup)

from rigpackage.rigIO import rigData

reload(rigData)

import maya.cmds as cmds

from rigpackage.rigmodule.fkmodule import crvFunc

reload(crvFunc)

from rigpackage.rigmodule.fkmodule import ctrlRig as cr

reload(cr)

from rigpackage.rigmodule.fkmodule import flapFkRig02 as ffkr

reload(ffkr)

from rigpackage.rigmodule.fkmodule import fkRig02 as fkr

reload(fkr)

from rigpackage.rigmodule.fkmodule import naming as N

reload(N)

from rigpackage.rigmodule.fkmodule import rigTools as rt

reload(rt)

from rigpackage.rigwip.eyemodule import eyemodule

reload(eyemodule)

from rigpackage.rigmodule.voljointmodule import volumeJointCreator as vj

reload(vj)

from rigpackage.rigwip import addMissingInfluence as ami

reload(ami)

from rigpackage.rigwip import connectMeshSkincluter
reload(connectMeshSkincluter)

from rigpackage.riglib import rigLibUtil as RigLib
reload(RigLib)
from rigpackage.rigIO import rigData
reload(rigData)


import pymel.core as pmc
import maya.mel as mel
import os


class CockroachBuild(rigAssetChayaSetup.AssetChayaCockroachSetup):

    Eyes_Rig = True
    RibbonLimb_Rig = True
    def __init__(self, rigType='', facial=False, *args, **kwargs):
        self.rigType = rigType
        self.facial = facial
        super(CockroachBuild, self).__init__(*args, **kwargs)

    def initialize(self):
        super(CockroachBuild, self).initialize()


    def moduleStructure(self):
        super(CockroachBuild, self).moduleStructure()
        #self.eyeLids_module()
        self.addRig_module()
        self.wing_module()
        #self.wing_module()
        #self.tail_module()
        #self.beak_module()
        # if self.rigType == 'animation' or 'deformation':
        #     self.armPad_module()
        #     self.shoulderCloak_module()
        #     self.dynamic_module()
        #     self.CloakFront_module()
        #     self.CloakBack_module()

        #     self.shoulderCloak_detail_module()


        # if self.rigType == 'deformation':
        #     self.vol_jnt_module()


    def post_build(self):
        super(CockroachBuild, self).post_build()
        #self.add_blendAttr_LegRollCtls()
        self.fix_interpType()
        #self.eyeLidsBlink_module()
        #rigData.readCtrlShape()
        rigData.readCtrlShape()
        # self.read_main_weight()
        #self.read_geo_weight()
        # self.read_proxy_geo_weight()
        # self.connect_mesh()
        # self.set_dyn_value()
        # self.parent_all_skinJnt()
        # self.parent_Geo_Grp()

    def parent_Geo_Grp(self):
        cmds.parent('*Geo_grp', 'Rig_Grp')

    def parent_all_skinJnt(self):

        for side in 'Lf', 'Rt':
            cmds.parent('{}_ShoulderCloak_0_Skin_Jnt'.format(side), '{}_Shoulder_0_Skin_Jnt'.format(side))
            cmds.parent('{}_ShoulderCloakOffset_0_Skin_Jnt'.format(side), '{}_ShoulderCloak_0_Skin_Jnt'.format(side))

            cmds.parent('{}_CloakBack0_0_Skin_Jnt'.format(side), '{}_CloakBackSpace0_0_Skin_Jnt'.format(side))
            cmds.parent('{}_CloakBackSpace0_0_Skin_Jnt'.format(side), 'Spine_3_Skin_Jnt')

        for i in range(5):
            cmds.parent('Lf_ShoulderCloakDetail{}_0_Skin_Jnt'.format(i), 'Lf_ShoulderCloakOffset_0_Skin_Jnt')
        for i in range(2):
            cmds.parent('Lf_CloakFront{}_0_Skin_Jnt'.format(i), 'Lf_CloakSpace{}_0_Skin_Jnt'.format(i))
            cmds.parent('Lf_CloakSpace{}_0_Skin_Jnt'.format(i), 'Lf_Shoulder_0_Skin_Jnt')


    def read_geo_weight(self):
        cmds.select(cmds.ls('*_ply', type='transform'))
        rigData.readSelectedWeight()

    def read_proxy_geo_weight(self):
        cmds.select(cmds.ls('*_proxy', type='transform'))
        rigData.readSelectedWeight()


    def read_main_weight(self):
        geo_list = []
        for mesh in cmds.listRelatives('*Geo_Grp', ad=1, type='mesh'):
            par = cmds.listRelatives(mesh, p=1)[0]
            geo_list.append(par)
        cmds.select(geo_list)
        rigData.readSelectedWeight()

    def fix_interpType(self):
        cons = cmds.ls(type='parentConstraint')
        for i in cons:
            if len(cmds.parentConstraint(i,q=1,wal=1)) >1:
                cmds.setAttr(i+'.interpType',2)

    def addRig_module(self):
        fkr.FkRig(name='Tail',
            jnt= rt.search_tmp_jnt('Tail'),
            no_tip=0,
            crvShape='circle',
            color='red',
            skin=1,
            gimbal=1,
            parent= 'Hip_0_Skin_Jnt',
            animGrp='Anim_Grp',
            skinGrp='Hip_0_Skin_Jnt',
            utilGrp='CtlJnt_Grp',
            stillGrp='Still_Grp',
            ikhGrp='IkHandle_Grp',
            spaceGrp='Orient_Grp',
            stretch=0,
            squash=0,
            lock_scale=0)


        for side in 'Lf','Rt':
            for part in ['Antenna','Palps0','Palps1']:
                fkr.FkRig(name='{}_{}'.format(side,part),
                                jnt=rt.search_tmp_jnt('{}_{}'.format(side,part)),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                skin=1,
                                gimbal=1,
                                parent= 'Head_0_Skin_Jnt',
                                animGrp='Anim_Grp',
                                skinGrp='Head_0_Skin_Jnt',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0)

            fkr.FkRig(name='{}_Mandible'.format(side),
                jnt= ['{}_Mandible_0_Tmp_Jnt'.format(side)],
                no_tip=0,
                crvShape='circle',
                color='red',
                skin=1,
                gimbal=1,
                parent= 'Head_0_Skin_Jnt'.format(side),
                animGrp='Anim_Grp',
                skinGrp='Head_0_Skin_Jnt'.format(side),
                utilGrp='CtlJnt_Grp',
                stillGrp='Still_Grp',
                ikhGrp='IkHandle_Grp',
                spaceGrp='Orient_Grp',
                stretch=0,
                squash=0,
                lock_scale=0)




            fkr.FkRig(name='{}_Tail'.format(side),
                jnt= rt.search_tmp_jnt('{}_Tail'.format(side)),
                no_tip=0,
                crvShape='circle',
                color='red',
                skin=1,
                gimbal=1,
                parent= 'Tail_4_Skin_Jnt',
                animGrp='Anim_Grp',
                skinGrp='Tail_4_Skin_Jnt',
                utilGrp='CtlJnt_Grp',
                stillGrp='Still_Grp',
                ikhGrp='IkHandle_Grp',
                spaceGrp='Orient_Grp',
                stretch=0,
                squash=0,
                lock_scale=0)


    def wing_module(self):
        for side in 'Lf','Rt':

            wing0 = fkr.FkRig(name='{}_Wing0'.format(side),
                                    jnt=rt.search_tmp_jnt('{}_Wing0'.format(side)),
                                    no_tip=0,
                                    crvShape='cube',
                                    color='lightBlue',
                                    skin=1,
                                    gimbal=1,
                                    parent='Spine_3_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='Spine_3_Skin_Jnt',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

            wing1 = fkr.FkRig(name='{}_Wing1'.format(side),
                                    jnt=['{}_Wing1_0_Tmp_Jnt'.format(side)],
                                    no_tip=0,
                                    crvShape='cube',
                                    color='lightBlue',
                                    skin=1,
                                    gimbal=1,
                                    parent='Spine_2_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='Spine_2_Skin_Jnt',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

            for i in range(2):
                wing1 = fkr.FkRig(name='{}_WingSub{}'.format(side,i),
                                        jnt=rt.search_tmp_jnt('{}_WingSub{}'.format(side,i)),
                                        no_tip=0,
                                        crvShape='cube',
                                        color='lightBlue',
                                        skin=1,
                                        gimbal=1,
                                        parent='{}_Wing1_0_Skin_Jnt'.format(side),
                                        animGrp='Anim_Grp',
                                        skinGrp='{}_Wing1_0_Skin_Jnt'.format(side),
                                        utilGrp='CtlJnt_Grp',
                                        stillGrp='Still_Grp',
                                        ikhGrp='IkHandle_Grp',
                                        spaceGrp='Orient_Grp',
                                        stretch=0,
                                        squash=0,
                                        lock_scale=0)

    def tail_module(self):
        fkr.FkRig(name='Tail',
                                    jnt=rt.search_tmp_jnt('Tail'),
                                    no_tip=0,
                                    crvShape='circle',
                                    color='red',
                                    skin=1,
                                    gimbal=1,
                                    parent='Hip_0_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='Hip_0_Skin_Jnt',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

    def add_blendAttr_LegRollCtls(self):

        for side in 'Lf', 'Rt':
            ik_ctrl = '{}_FeetRoll_0_Ik_Ctl'.format(side)
            cmds.delete('{}_LegRollCons_pGrp_orientConstraint1'.format(side))
            constraint = cmds.parentConstraint('{}_LegAim_0_Ik_Jnt'.format(side), '{}_Feet_0_Ik_CtlGmb'.format(side),
                                             '{}_LegRollCons_pGrp'.format(side), mo=True, st=['x', 'y', 'z'])[0]
            cmds.setAttr('{}.interpType'.format(constraint),2)

            pmc.addAttr(ik_ctrl, ln='rollActive', dv=1, max=1, min=0, k=1)

            cmds.connectAttr('{}.rollActive'.format(ik_ctrl), '{}.{}_LegAim_0_Ik_JntW0'.format(constraint, side))
            rv_node = cmds.createNode('reverse', n='{}_Roll_0_Ik_Rv'.format(side))
            cmds.connectAttr('{}.rollActive'.format(ik_ctrl), '{}.inputX'.format(rv_node))
            cmds.connectAttr('{}.outputX'.format(rv_node), '{}.{}_Feet_0_Ik_CtlGmbW1'.format(constraint, side))

            ###Feet Ctl
            ikRollCtl_offGrp ='{}_FeetRoll_0_Ik_CtlOriGrp'.format(side)
            cmds.delete('{}_FeetRoll_0_Ik_CtlOriGrp_orientConstraint1'.format(side))

            ikRollCtl_offGrp_constraint = cmds.parentConstraint('{}_LegAim_0_Ik_Jnt'.format(side), '{}_Feet_0_Ik_CtlGmb'.format(side),
                                             ikRollCtl_offGrp, mo=True, st=['x', 'y', 'z'])[0]
            cmds.setAttr('{}.interpType'.format(ikRollCtl_offGrp_constraint),2)
            cmds.connectAttr('{}.rollActive'.format(ik_ctrl), '{}.{}_LegAim_0_Ik_JntW0'.format(ikRollCtl_offGrp_constraint, side))
            cmds.connectAttr('{}.outputX'.format(rv_node), '{}.{}_Feet_0_Ik_CtlGmbW1'.format(ikRollCtl_offGrp_constraint, side))


    def shoulderCloak_module(self):
        self.shoulderCloak_R = ffkr.FlapFkRig(name='ShoulderCloak',
                                       pos='Lf',
                                       jnt=rt.search_tmp_jnt('ShoulderCloak', 'Lf'),
                                       crvShape='cube',
                                       color='blue',
                                       skin=1,
                                       gimbal=1,
                                       parent='Lf_Shoulder_0_Skin_Jnt',
                                       animGrp='Anim_Grp',
                                       skinGrp='SkinJnt_Grp',
                                       utilGrp='CtlJnt_Grp',
                                       stillGrp='Still_Grp',
                                       ikhGrp='IkHandle_Grp',
                                       spaceGrp='Orient_Grp',
                                       stretch=1,
                                       squash=1,
                                       start='Lf_Shoulder_0_Skin_Jnt',
                                       end='Lf_ArmRib_4_Skin_Jnt',
                                       aim_axis='x',
                                       up_axis='y',
                                       setting=dict(maxValue=[-10, 80],
                                                    minValue=[80, -80]))

        self.shoulderCloak_L = ffkr.FlapFkRig(name='ShoulderCloak',
                                       pos='Rt',
                                       jnt=rt.search_tmp_jnt('ShoulderCloak', 'Rt'),
                                       crvShape='cube',
                                       color='blue',
                                       skin=1,
                                       gimbal=1,
                                       parent='Rt_Shoulder_0_Skin_Jnt',
                                       animGrp='Anim_Grp',
                                       skinGrp='SkinJnt_Grp',
                                       utilGrp='CtlJnt_Grp',
                                       stillGrp='Still_Grp',
                                       ikhGrp='IkHandle_Grp',
                                       spaceGrp='Orient_Grp',
                                       stretch=1,
                                       squash=1,
                                       start='Rt_Shoulder_0_Skin_Jnt',
                                       end='Rt_ArmRib_4_Skin_Jnt',
                                       aim_axis='x',
                                       up_axis='y',
                                       setting=dict(maxValue=[-10, 60],
                                                    minValue=[45, -60]))

        for side in 'Lf', 'Rt':
            self.shoulderCloak_offset = fkr.FkRig(name='{}_ShoulderCloakOffset'.format(side),
                                    jnt=rt.search_tmp_jnt('{}_ShoulderCloak'.format(side)),
                                    no_tip=0,
                                    crvShape='cube',
                                    color='lightBlue',
                                    skin=1,
                                    gimbal=1,
                                    parent='{}_ShoulderCloak_0_Skin_Jnt'.format(side),
                                    animGrp='Anim_Grp',
                                    skinGrp='SkinJnt_Grp',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

    def CloakFront_module(self):
        self.Cloak0_space = fkr.FkRig(name='Lf_CloakSpace0',
                                jnt=rt.search_tmp_jnt('Lf_CloakSpace0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                skin=1,
                                gimbal=0,
                                parent='Spine_3_Skin_Jnt',
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0)

        self.CloakFront0 = fkr.FkRig(name='Lf_CloakFront0',
                                jnt=rt.search_tmp_jnt('Lf_CloakFront0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                space_default= 1,
                                skin=1,
                                gimbal=1,
                                space=['Lf_CloakSpace0_0_Skin_Jnt', 'CtlJnt_Grp'],
                                parent='Lf_CloakSpace0_0_Skin_Jnt',
                                blend_space=1,
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0,
                                dyn=1,
                                dyn_ctrl=self.hair_switch.ctrl.pynode,
                                nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                    )

        self.Cloak1_space = fkr.FkRig(name='Lf_CloakSpace1',
                                      jnt=rt.search_tmp_jnt('Lf_CloakSpace1'),
                                      no_tip=0,
                                      crvShape='circle',
                                      color='red',
                                      skin=1,
                                      gimbal=0,
                                      parent='Spine_3_Skin_Jnt',
                                      animGrp='Anim_Grp',
                                      skinGrp='SkinJnt_Grp',
                                      utilGrp='CtlJnt_Grp',
                                      stillGrp='Still_Grp',
                                      ikhGrp='IkHandle_Grp',
                                      spaceGrp='Orient_Grp',
                                      stretch=0,
                                      squash=0,
                                      lock_scale=0)

        self.CloakFront1 = fkr.FkRig(name='Lf_CloakFront1',
                                     jnt=rt.search_tmp_jnt('Lf_CloakFront1'),
                                     no_tip=0,
                                     crvShape='circle',
                                     color='red',
                                     space_default=1,
                                     skin=1,
                                     gimbal=1,
                                     space=['Lf_CloakSpace1_0_Skin_Jnt', 'CtlJnt_Grp'],
                                     parent='Lf_CloakSpace1_0_Skin_Jnt',
                                     blend_space=1,
                                     animGrp='Anim_Grp',
                                     skinGrp='SkinJnt_Grp',
                                     utilGrp='CtlJnt_Grp',
                                     stillGrp='Still_Grp',
                                     ikhGrp='IkHandle_Grp',
                                     spaceGrp='Orient_Grp',
                                     stretch=0,
                                     squash=0,
                                     lock_scale=0,
                                     dyn=1,
                                     dyn_ctrl=self.hair_switch.ctrl.pynode,
                                     nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                     nucleus=self.CloakFront0.nucleus.pynode,  # nucleus node to connect with
                                     )

    def connect_mesh(self):
        connectMeshSkincluter.connect_ctl_to_mesh([
            ['cloak02_proxy', [
                'Lf_CloakSpace0_0_Fk_CtlGrp',
                'Lf_CloakSpace1_0_Fk_CtlGrp',
                'Lf_CloakBackSpace0_0_Fk_CtlGrp',
                'Rt_CloakBackSpace0_0_Fk_CtlGrp',

            ]]])

    def dynamic_module(self):
        self.hair_switch = cr.Ctrl(name='cloakDyn', crvShape='stick', des=N.SWITCH,
                                   gimbal=0, color='lightBlue', obj='Head_0_Anim_Jnt', size=10)
        self.hair_switch.zrGrp.pynode.setParent('Anim_Grp')
        pmc.parentConstraint('Head_0_Anim_Jnt', self.hair_switch.zrGrp.pynode, mo=1)
        self.hair_switch.ctrl.lock_hide_attr('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

    def CloakBack_module(self):
        self.CloakBack0_space_L = fkr.FkRig(name='Lf_CloakBackSpace0',
                                jnt=rt.search_tmp_jnt('Lf_CloakBackSpace0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                skin=1,
                                gimbal=0,
                                parent='Spine_3_Skin_Jnt',
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0)

        self.CloakBack0_L = fkr.FkRig(name='Lf_CloakBack0',
                                jnt=rt.search_tmp_jnt('Lf_CloakBack0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                space_default= 1,
                                skin=1,
                                gimbal=1,
                                space=['Lf_CloakBackSpace0_0_Skin_Jnt', 'CtlJnt_Grp'],
                                parent='Lf_CloakBackSpace0_0_Skin_Jnt',
                                blend_space=1,
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0,
                                dyn=1,
                                dyn_ctrl=self.hair_switch.ctrl.pynode,
                                nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                nucleus=self.CloakFront0.nucleus.pynode,  # nucleus node to connect with

                                     )

        self.CloakBack0_space_R = fkr.FkRig(name='Rt_CloakBackSpace0',
                                      jnt=rt.search_tmp_jnt('Rt_CloakBackSpace0'),
                                      no_tip=0,
                                      crvShape='circle',
                                      color='blue',
                                      skin=1,
                                      gimbal=0,
                                      parent='Spine_3_Skin_Jnt',
                                      animGrp='Anim_Grp',
                                      skinGrp='SkinJnt_Grp',
                                      utilGrp='CtlJnt_Grp',
                                      stillGrp='Still_Grp',
                                      ikhGrp='IkHandle_Grp',
                                      spaceGrp='Orient_Grp',
                                      stretch=0,
                                      squash=0,
                                      lock_scale=0)

        self.CloakBack0_R = fkr.FkRig(name='Rt_CloakBack0',
                                     jnt=rt.search_tmp_jnt('Rt_CloakBack0'),
                                     no_tip=0,
                                     crvShape='circle',
                                     color='blue',
                                     space_default=1,
                                     skin=1,
                                     gimbal=1,
                                     space=['Rt_CloakBackSpace0_0_Skin_Jnt', 'CtlJnt_Grp'],
                                     parent='Rt_CloakBackSpace0_0_Skin_Jnt',
                                     blend_space=1,
                                     animGrp='Anim_Grp',
                                     skinGrp='SkinJnt_Grp',
                                     utilGrp='CtlJnt_Grp',
                                     stillGrp='Still_Grp',
                                     ikhGrp='IkHandle_Grp',
                                     spaceGrp='Orient_Grp',
                                     stretch=0,
                                     squash=0,
                                     lock_scale=0,
                                     dyn=1,
                                     dyn_ctrl=self.hair_switch.ctrl.pynode,
                                     nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                     nucleus=self.CloakFront0.nucleus.pynode,  # nucleus node to connect with
                                     )

    def shoulderCloak_detail_module(self):

        for i in range(5):
            self.shoulderCloak_detail = fkr.FkRig(name='Lf_ShoulderCloakDetail{}'.format(i),
                                    jnt=rt.search_tmp_jnt('Lf_ShoulderCloakDetail{}'.format(i)),
                                    no_tip=0,
                                    crvShape='cube',
                                    color='red',
                                    skin=1,
                                    gimbal=1,
                                    parent='Lf_ShoulderCloakOffset_0_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='SkinJnt_Grp',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

    def set_dyn_value(self):
        dyn = cmds.ls(type='hairSystem')
        cmds.sets(dyn, n='hairSystem_0_Anim_Set')
        for d in dyn:
            cmds.setAttr("{}.attractionScale[0].attractionScale_Position".format(d), 0.75)
            cmds.setAttr("{}.attractionDamp".format(d), 0.5)

        for ctrl in [self.hair_switch.ctrl.name]:
            for attr in cmds.listAttr(ctrl, ud=1):
                if 'CurveAttract' in attr:
                    cmds.setAttr('{}.{}'.format(ctrl, attr), .75)

