from ... import rigAssetChayaSetup

reload(rigAssetChayaSetup)

from rigpackage.rigIO import rigData

reload(rigData)

import maya.cmds as cmds

from rigpackage.rigmodule.fkmodule import crvFunc

reload(crvFunc)

from rigpackage.rigmodule.fkmodule import ctrlRig as cr

reload(cr)

from rigpackage.rigmodule.fkmodule import flapFkRig02 as ffkr

reload(ffkr)

from rigpackage.rigmodule.fkmodule import fkRig02 as fkr

reload(fkr)

from rigpackage.rigmodule.fkmodule import naming as N

reload(N)

from rigpackage.rigmodule.fkmodule import rigTools as rt

reload(rt)

from rigpackage.rigwip.eyemodule import eyemodule

reload(eyemodule)

from rigpackage.rigmodule.voljointmodule import volumeJointCreator as vj

reload(vj)

from rigpackage.rigwip import addMissingInfluence as ami

reload(ami)

from rigpackage.rigwip import connectMeshSkincluter
reload(connectMeshSkincluter)

from rigpackage.riglib import rigLibUtil as RigLib
reload(RigLib)
from rigpackage.rigIO import rigData
reload(rigData)

from rigpackage.riglib import rigLibAttr as RigLibAttr
reload(RigLibAttr)


import pymel.core as pmc
import maya.mel as mel
import os


##test push ignore pyc

class BirdBuild(rigAssetChayaSetup.AssetChayaBirdSetup):

    Eyes_Rig = True
    RibbonLimb_Rig = True
    def __init__(self, rigType='', facial=False, *args, **kwargs):
        self.rigType = rigType
        self.facial = facial
        super(BirdBuild, self).__init__(*args, **kwargs)

    def initialize(self):
        super(BirdBuild, self).initialize()


    def moduleStructure(self):
        super(BirdBuild, self).moduleStructure()
        #self.eyeLids_module()
        self.tail_module()
        self.beak_module()
        self.finger_module()
        self.wing_module()

        self.toes_module()

        # if self.rigType == 'animation' or 'deformation':
        #     self.armPad_module()
        #     self.shoulderCloak_module()
        #     self.dynamic_module()
        #     self.CloakFront_module()
        #     self.CloakBack_module()

        #     self.shoulderCloak_detail_module()


        # if self.rigType == 'deformation':
        #     self.vol_jnt_module()


    def post_build(self):
        super(BirdBuild, self).post_build()
        self.add_blendAttr_LegRollCtls()
        self.fix_interpType()
        #self.eyeLidsBlink_module()
        #rigData.readCtrlShape()
        rigData.readCtrlShape()
        # self.read_main_weight()
        self.read_geo_weight()
        self.wing_constraint()
        self.hide_unusedCtrl()
        self.hide_attr()
        # self.read_proxy_geo_weight()
        # self.connect_mesh()
        # self.set_dyn_value()
        # self.parent_all_skinJnt()
        # self.parent_Geo_Grp

    def toes_module(self):
        for side in 'Lf', 'Rt':
            for i in range(3):
                fkr.FkRig(name='{}_Toe{}'.format(side, i),
                          jnt=rt.search_tmp_jnt('{}_Toe{}'.format(side, i)),
                          no_tip=1,
                          crvShape='circle',
                          color='yellow',
                          skin=1,
                          gimbal=1,
                          parent='{}_Feet_0_Skin_Jnt'.format(side),
                          animGrp='Anim_Grp',
                          skinGrp='{}_Feet_0_Skin_Jnt'.format(side),
                          utilGrp='CtlJnt_Grp',
                          stillGrp='Still_Grp',
                          ikhGrp='IkHandle_Grp',
                          spaceGrp='Orient_Grp',
                          stretch=0,
                          squash=0,
                          lock_scale=0)

            fkr.FkRig(name='{}_Toe3'.format(side),
                      jnt=rt.search_tmp_jnt('{}_Toe3'.format(side)),
                      no_tip=1,
                      crvShape='circle',
                      color='yellow',
                      skin=1,
                      gimbal=1,
                      parent='{}_LowLegRib_0_Skin_Jnt'.format(side),
                      animGrp='Anim_Grp',
                      skinGrp='{}_LowLegRib_0_Skin_Jnt'.format(side),
                      utilGrp='CtlJnt_Grp',
                      stillGrp='Still_Grp',
                      ikhGrp='IkHandle_Grp',
                      spaceGrp='Orient_Grp',
                      stretch=0,
                      squash=0,
                      lock_scale=0)



    def wing_constraint(self):

        for side in 'Lf', 'Rt':
            con = cmds.orientConstraint('{}_Wing1_0_Skin_Jnt'.format(side), '{}_Wing2_0_Skin_Jnt'.format(side),
                                        '{}_Sec0_0_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

            con = cmds.orientConstraint('{}_Wing2_0_Skin_Jnt'.format(side), '{}_Wing3_0_Skin_Jnt'.format(side),
                                        '{}_Sec1_0_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

            cmds.pointConstraint('{}_Fin_0_Skin_Jnt'.format(side), '{}_Fin_1_Skin_Jnt'.format(side),
                                        '{}_Sec1_0_Fk_CtlGrp'.format(side), mo=True)

            cmds.pointConstraint('{}_ElbowRib_2_Skin_Jnt'.format(side),
                                        '{}_Sec0_0_Fk_CtlGrp'.format(side), mo=True)

            con = cmds.orientConstraint('{}_Wing1_1_Skin_Jnt'.format(side), '{}_Wing2_1_Skin_Jnt'.format(side),
                                        '{}_Sec0_1_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

            con = cmds.orientConstraint('{}_Wing2_1_Skin_Jnt'.format(side), '{}_Wing3_1_Skin_Jnt'.format(side),
                                        '{}_Sec1_1_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

    def hide_attr(self):
        for side in 'Lf', 'Rt':
            RigLibAttr.lockHideAttributes("{}_Elbow_0_Fk_Ctl".format(side), t=1, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Wrist_0_Fk_Ctl".format(side), t=1, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Arm_0_Fk_Ctl".format(side), t=1, r=0, s=1, v=1)

            RigLibAttr.lockHideAttributes("{}_Wing0_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Wing1_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Wing2_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Wing3_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)

            RigLibAttr.lockHideAttributes("{}_WingTip_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Fin_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)
            RigLibAttr.lockHideAttributes("{}_Fin_0_Fk_Ctl".format(side), t=0, r=0, s=1, v=1)

            pmc.addAttr("{}_Wing0_0_Fk_Ctl".format(side), ln='bend', dv=0, k=1)
            pmc.addAttr("{}_Wing1_0_Fk_Ctl".format(side), ln='bend', dv=0, k=1)
            pmc.addAttr("{}_Wing2_0_Fk_Ctl".format(side), ln='bend', dv=0, k=1)
            pmc.addAttr("{}_Wing3_0_Fk_Ctl".format(side), ln='bend', dv=0, k=1)

            cmds.connectAttr('{}_Wing0_0_Fk_Ctl.bend'.format(side), '{}_Wing0_1_Fk_CtlDrvGrp.rx'.format(side))
            cmds.connectAttr('{}_Wing1_0_Fk_Ctl.bend'.format(side), '{}_Wing1_1_Fk_CtlDrvGrp.rx'.format(side))
            cmds.connectAttr('{}_Wing2_0_Fk_Ctl.bend'.format(side), '{}_Wing2_1_Fk_CtlDrvGrp.rx'.format(side))
            cmds.connectAttr('{}_Wing3_0_Fk_Ctl.bend'.format(side), '{}_Wing3_1_Fk_CtlDrvGrp.rx'.format(side))
            cmds.setAttr("{}_Elbow_0_Fk_Ctl.squash".format(side), l=1, k=0, ch=0)
            # cmds.setAttr("{}_Elbow_0_Fk_Ctl.rx".format(side), l=1, k=0, ch=0)
            # cmds.setAttr("{}_Elbow_0_Fk_Ctl.rz".format(side), l=1, k=0, ch=0)
            cmds.setAttr("{}_Wrist_0_Fk_Ctl.squash".format(side), l=1, k=0, ch=0)
            cmds.setAttr("{}_Arm_0_Fk_Ctl.squash".format(side), l=1, k=0, ch=0)



    def hide_unusedCtrl(self):
        for side in 'Lf', 'Rt':
            cmds.setAttr("{}_Sec0_0_FkCtl_Grp.v".format(side), 0);
            cmds.setAttr("{}_Sec1_0_FkCtl_Grp.v".format(side), 0);
            cmds.setAttr("{}_Wing0_1_Fk_CtlGrp.v".format(side), 0);
            cmds.setAttr("{}_Wing1_1_Fk_CtlGrp.v".format(side), 0);
            cmds.setAttr("{}_Wing2_1_Fk_CtlGrp.v".format(side), 0);
            cmds.setAttr("{}_Wing3_1_Fk_CtlGrp.v".format(side), 0);



    def parent_Geo_Grp(self):
        cmds.parent('*Geo_grp', 'Rig_Grp')

    def parent_all_skinJnt(self):

        for side in 'Lf', 'Rt':
            cmds.parent('{}_ShoulderCloak_0_Skin_Jnt'.format(side), '{}_Shoulder_0_Skin_Jnt'.format(side))
            cmds.parent('{}_ShoulderCloakOffset_0_Skin_Jnt'.format(side), '{}_ShoulderCloak_0_Skin_Jnt'.format(side))

            cmds.parent('{}_CloakBack0_0_Skin_Jnt'.format(side), '{}_CloakBackSpace0_0_Skin_Jnt'.format(side))
            cmds.parent('{}_CloakBackSpace0_0_Skin_Jnt'.format(side), 'Spine_3_Skin_Jnt')

        for i in range(5):
            cmds.parent('Lf_ShoulderCloakDetail{}_0_Skin_Jnt'.format(i), 'Lf_ShoulderCloakOffset_0_Skin_Jnt')
        for i in range(2):
            cmds.parent('Lf_CloakFront{}_0_Skin_Jnt'.format(i), 'Lf_CloakSpace{}_0_Skin_Jnt'.format(i))
            cmds.parent('Lf_CloakSpace{}_0_Skin_Jnt'.format(i), 'Lf_Shoulder_0_Skin_Jnt')


    def read_geo_weight(self):
        cmds.select(cmds.ls('*_geo', type='transform'))
        rigData.readSelectedWeight()

    def read_proxy_geo_weight(self):
        cmds.select(cmds.ls('*_proxy', type='transform'))
        rigData.readSelectedWeight()


    def read_main_weight(self):
        geo_list = []
        for mesh in cmds.listRelatives('*Geo_Grp', ad=1, type='mesh'):
            par = cmds.listRelatives(mesh, p=1)[0]
            geo_list.append(par)
        cmds.select(geo_list)
        rigData.readSelectedWeight()

    def fix_interpType(self):
        cons = cmds.ls(type='parentConstraint')
        for i in cons:
            if len(cmds.parentConstraint(i,q=1,wal=1)) >1:
                cmds.setAttr(i+'.interpType',2)

    def wing_module(self):
        for side in 'Lf','Rt':
            fkr.FkRig(name='{}_WingTip'.format(side),
                                                    jnt=['{}_WingTip_0_Tmp_Jnt'.format(side)],
                                                    no_tip=0,
                                                    crvShape='sphere',
                                                    color='lightBlue',
                                                    skin=1,
                                                    gimbal=1,
                                                    parent= '{}_Fin_2_Skin_Jnt'.format(side),
                                                    animGrp='Anim_Grp',
                                                    skinGrp='{}_Fin_2_Skin_Jnt'.format(side),
                                                    utilGrp='CtlJnt_Grp',
                                                    stillGrp='Still_Grp',
                                                    ikhGrp='IkHandle_Grp',
                                                    spaceGrp='Orient_Grp',
                                                    stretch=0,
                                                    squash=0,
                                                    lock_scale=0)

            par_wing = ['{}_Arm_0_Skin_Jnt'.format(side),'{}_Elbow_0_Skin_Jnt'.format(side),'{}_Wrist_0_Skin_Jnt'.format(side),'{}_WingTip_0_Skin_Jnt'.format(side)]
            for i in range(4):
                self.feather = fkr.FkRig(name='{}_Wing{}'.format(side,i),
                                        # jnt=['{}_Wing{}_0_Tmp_Jnt'.format(side,i)],
                                        jnt=rt.search_tmp_jnt('{}_Wing{}'.format(side, i)),
                                        no_tip=1,
                                        crvShape='cube',
                                        color='lightBlue',
                                        skin=1,
                                        gimbal=1,
                                        parent=par_wing[i],
                                        animGrp='Anim_Grp',
                                        skinGrp=par_wing[i],
                                        utilGrp='CtlJnt_Grp',
                                        stillGrp='Still_Grp',
                                        ikhGrp='IkHandle_Grp',
                                        spaceGrp='Orient_Grp',
                                        stretch=0,
                                        squash=0,
                                        lock_scale=0)
                #self.ArmPad.fk_joint_list[-1]

            for i in range(2):
                feather = fkr.FkRig(name='{}_Sec{}'.format(side,i),
                                        jnt=rt.search_tmp_jnt('{}_Sec{}'.format(side, i)),
                                        no_tip=1,
                                        crvShape='cube',
                                        color='pink',
                                        skin=1,
                                        gimbal=1,
                                        skinGrp='SkinJnt_Grp',
                                        animGrp='Anim_Grp',
                                        utilGrp='CtlJnt_Grp',
                                        stillGrp='Still_Grp',
                                        ikhGrp='IkHandle_Grp',
                                        spaceGrp='Orient_Grp',
                                        stretch=0,
                                        squash=0,
                                        lock_scale=0)


    def beak_module(self):
        for side in 'Up','Down':

            feather = fkr.FkRig(name='Beak{}'.format(side),
                                    jnt=['Beak{}_0_Tmp_Jnt'.format(side)],
                                    no_tip=0,
                                    crvShape='cube',
                                    color='lightBlue',
                                    skin=1,
                                    gimbal=1,
                                    parent='Head_0_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='Head_0_Skin_Jnt',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

    def tail_module(self):
        fkr.FkRig(name='Tail',
                                    jnt=rt.search_tmp_jnt('Tail'),
                                    no_tip=1,
                                    crvShape='circle',
                                    color='red',
                                    skin=1,
                                    gimbal=1,
                                    parent='Hip_0_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='Hip_0_Skin_Jnt',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

        fkr.FkRig(name='TailDetail',
                  jnt=rt.search_tmp_jnt('TailDetail0'),
                  no_tip=1,
                  crvShape='circle',
                  color='yellow',
                  skin=1,
                  gimbal=1,
                  parent='Tail_2_Skin_Jnt',
                  animGrp='Anim_Grp',
                  skinGrp='Tail_2_Skin_Jnt',
                  utilGrp='CtlJnt_Grp',
                  stillGrp='Still_Grp',
                  ikhGrp='IkHandle_Grp',
                  spaceGrp='Orient_Grp',
                  stretch=0,
                  squash=0,
                  lock_scale=0)

        for side in 'Lf', 'Rt':
            fkr.FkRig(name='{}_TailDetail'.format(side),
                      jnt=rt.search_tmp_jnt('{}_TailDetail0'.format(side)),
                      no_tip=1,
                      crvShape='circle',
                      color='yellow',
                      skin=1,
                      gimbal=1,
                      parent='Tail_2_Skin_Jnt',
                      animGrp='Anim_Grp',
                      skinGrp='Tail_2_Skin_Jnt',
                      utilGrp='CtlJnt_Grp',
                      stillGrp='Still_Grp',
                      ikhGrp='IkHandle_Grp',
                      spaceGrp='Orient_Grp',
                      stretch=0,
                      squash=0,
                      lock_scale=0)

            fkr.FkRig(name='{}_TailSub'.format(side),
                      jnt=rt.search_tmp_jnt('{}_TailSub0'.format(side)),
                      no_tip=1,
                      crvShape='circle',
                      color='pink',
                      skin=1,
                      gimbal=1,
                      parent='Tail_2_Skin_Jnt',
                      animGrp='Anim_Grp',
                      skinGrp='Tail_2_Skin_Jnt',
                      utilGrp='CtlJnt_Grp',
                      stillGrp='Still_Grp',
                      ikhGrp='IkHandle_Grp',
                      spaceGrp='Orient_Grp',
                      stretch=0,
                      squash=0,
                      lock_scale=0)


        cmds.parentConstraint("Tail_3_Skin_Jnt", "TailDetail_1_Fk_CtlGrp", mo=True)

        for side in 'Lf', 'Rt':
            cmds.parentConstraint("Tail_3_Skin_Jnt", "{}_TailDetail_1_Fk_CtlGrp".format(side), mo=True)


        #sharing constriant
        for side in 'Lf', 'Rt':
            con = cmds.orientConstraint('TailDetail_1_Skin_Jnt', '{}_TailDetail_1_Skin_Jnt'.format(side),
                                        '{}_TailSub_1_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

            con = cmds.orientConstraint('TailDetail_2_Skin_Jnt', '{}_TailDetail_2_Skin_Jnt'.format(side),
                                        '{}_TailSub_2_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

            con = cmds.orientConstraint('TailDetail_3_Skin_Jnt', '{}_TailDetail_3_Skin_Jnt'.format(side),
                                        '{}_TailSub_3_Fk_CtlGrp'.format(side), mo=True)
            cmds.setAttr("{}.interpType".format(con[0]), 2);

            cmds.setAttr("{}_TailSub_0_Fk_CtlGrp.v".format(side), 0)


    def finger_module(self):
        for side in 'Lf', 'Rt':
            fkr.FkRig(name='{}_Fin'.format(side),
                                                    # jnt=['{}_Fin_0_Tmp_Jnt'.format(side)],
                                                    jnt=rt.search_tmp_jnt('{}_Fin'.format(side)),
                                                    no_tip=1,
                                                    crvShape='circle',
                                                    color='lightBlue',
                                                    skin=1,
                                                    gimbal=1,
                                                    parent= '{}_Hand_0_Skin_Jnt'.format(side),
                                                    animGrp='Anim_Grp',
                                                    skinGrp='{}_Hand_0_Skin_Jnt'.format(side),
                                                    utilGrp='CtlJnt_Grp',
                                                    stillGrp='Still_Grp',
                                                    ikhGrp='IkHandle_Grp',
                                                    spaceGrp='Orient_Grp',
                                                    stretch=0,
                                                    squash=0,
                                                    lock_scale=0)


    def add_blendAttr_LegRollCtls(self):

        for side in 'Lf', 'Rt':
            ik_ctrl = '{}_FeetRoll_0_Ik_Ctl'.format(side)
            cmds.delete('{}_LegRollCons_pGrp_orientConstraint1'.format(side))
            constraint = cmds.parentConstraint('{}_LegAim_0_Ik_Jnt'.format(side), '{}_Feet_0_Ik_CtlGmb'.format(side),
                                             '{}_LegRollCons_pGrp'.format(side), mo=True, st=['x', 'y', 'z'])[0]
            cmds.setAttr('{}.interpType'.format(constraint),2)

            pmc.addAttr(ik_ctrl, ln='rollActive', dv=1, max=1, min=0, k=1)

            cmds.connectAttr('{}.rollActive'.format(ik_ctrl), '{}.{}_LegAim_0_Ik_JntW0'.format(constraint, side))
            rv_node = cmds.createNode('reverse', n='{}_Roll_0_Ik_Rv'.format(side))
            cmds.connectAttr('{}.rollActive'.format(ik_ctrl), '{}.inputX'.format(rv_node))
            cmds.connectAttr('{}.outputX'.format(rv_node), '{}.{}_Feet_0_Ik_CtlGmbW1'.format(constraint, side))

            ###Feet Ctl
            ikRollCtl_offGrp ='{}_FeetRoll_0_Ik_CtlOriGrp'.format(side)
            cmds.delete('{}_FeetRoll_0_Ik_CtlOriGrp_orientConstraint1'.format(side))

            ikRollCtl_offGrp_constraint = cmds.parentConstraint('{}_LegAim_0_Ik_Jnt'.format(side), '{}_Feet_0_Ik_CtlGmb'.format(side),
                                             ikRollCtl_offGrp, mo=True, st=['x', 'y', 'z'])[0]
            cmds.setAttr('{}.interpType'.format(ikRollCtl_offGrp_constraint),2)
            cmds.connectAttr('{}.rollActive'.format(ik_ctrl), '{}.{}_LegAim_0_Ik_JntW0'.format(ikRollCtl_offGrp_constraint, side))
            cmds.connectAttr('{}.outputX'.format(rv_node), '{}.{}_Feet_0_Ik_CtlGmbW1'.format(ikRollCtl_offGrp_constraint, side))


    def shoulderCloak_module(self):
        self.shoulderCloak_R = ffkr.FlapFkRig(name='ShoulderCloak',
                                       pos='Lf',
                                       jnt=rt.search_tmp_jnt('ShoulderCloak', 'Lf'),
                                       crvShape='cube',
                                       color='blue',
                                       skin=1,
                                       gimbal=1,
                                       parent='Lf_Shoulder_0_Skin_Jnt',
                                       animGrp='Anim_Grp',
                                       skinGrp='SkinJnt_Grp',
                                       utilGrp='CtlJnt_Grp',
                                       stillGrp='Still_Grp',
                                       ikhGrp='IkHandle_Grp',
                                       spaceGrp='Orient_Grp',
                                       stretch=1,
                                       squash=1,
                                       start='Lf_Shoulder_0_Skin_Jnt',
                                       end='Lf_ArmRib_4_Skin_Jnt',
                                       aim_axis='x',
                                       up_axis='y',
                                       setting=dict(maxValue=[-10, 80],
                                                    minValue=[80, -80]))

        self.shoulderCloak_L = ffkr.FlapFkRig(name='ShoulderCloak',
                                       pos='Rt',
                                       jnt=rt.search_tmp_jnt('ShoulderCloak', 'Rt'),
                                       crvShape='cube',
                                       color='blue',
                                       skin=1,
                                       gimbal=1,
                                       parent='Rt_Shoulder_0_Skin_Jnt',
                                       animGrp='Anim_Grp',
                                       skinGrp='SkinJnt_Grp',
                                       utilGrp='CtlJnt_Grp',
                                       stillGrp='Still_Grp',
                                       ikhGrp='IkHandle_Grp',
                                       spaceGrp='Orient_Grp',
                                       stretch=1,
                                       squash=1,
                                       start='Rt_Shoulder_0_Skin_Jnt',
                                       end='Rt_ArmRib_4_Skin_Jnt',
                                       aim_axis='x',
                                       up_axis='y',
                                       setting=dict(maxValue=[-10, 60],
                                                    minValue=[45, -60]))

        for side in 'Lf', 'Rt':
            self.shoulderCloak_offset = fkr.FkRig(name='{}_ShoulderCloakOffset'.format(side),
                                    jnt=rt.search_tmp_jnt('{}_ShoulderCloak'.format(side)),
                                    no_tip=0,
                                    crvShape='cube',
                                    color='lightBlue',
                                    skin=1,
                                    gimbal=1,
                                    parent='{}_ShoulderCloak_0_Skin_Jnt'.format(side),
                                    animGrp='Anim_Grp',
                                    skinGrp='SkinJnt_Grp',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

    def CloakFront_module(self):
        self.Cloak0_space = fkr.FkRig(name='Lf_CloakSpace0',
                                jnt=rt.search_tmp_jnt('Lf_CloakSpace0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                skin=1,
                                gimbal=0,
                                parent='Spine_3_Skin_Jnt',
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0)

        self.CloakFront0 = fkr.FkRig(name='Lf_CloakFront0',
                                jnt=rt.search_tmp_jnt('Lf_CloakFront0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                space_default= 1,
                                skin=1,
                                gimbal=1,
                                space=['Lf_CloakSpace0_0_Skin_Jnt', 'CtlJnt_Grp'],
                                parent='Lf_CloakSpace0_0_Skin_Jnt',
                                blend_space=1,
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0,
                                dyn=1,
                                dyn_ctrl=self.hair_switch.ctrl.pynode,
                                nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                    )

        self.Cloak1_space = fkr.FkRig(name='Lf_CloakSpace1',
                                      jnt=rt.search_tmp_jnt('Lf_CloakSpace1'),
                                      no_tip=0,
                                      crvShape='circle',
                                      color='red',
                                      skin=1,
                                      gimbal=0,
                                      parent='Spine_3_Skin_Jnt',
                                      animGrp='Anim_Grp',
                                      skinGrp='SkinJnt_Grp',
                                      utilGrp='CtlJnt_Grp',
                                      stillGrp='Still_Grp',
                                      ikhGrp='IkHandle_Grp',
                                      spaceGrp='Orient_Grp',
                                      stretch=0,
                                      squash=0,
                                      lock_scale=0)

        self.CloakFront1 = fkr.FkRig(name='Lf_CloakFront1',
                                     jnt=rt.search_tmp_jnt('Lf_CloakFront1'),
                                     no_tip=0,
                                     crvShape='circle',
                                     color='red',
                                     space_default=1,
                                     skin=1,
                                     gimbal=1,
                                     space=['Lf_CloakSpace1_0_Skin_Jnt', 'CtlJnt_Grp'],
                                     parent='Lf_CloakSpace1_0_Skin_Jnt',
                                     blend_space=1,
                                     animGrp='Anim_Grp',
                                     skinGrp='SkinJnt_Grp',
                                     utilGrp='CtlJnt_Grp',
                                     stillGrp='Still_Grp',
                                     ikhGrp='IkHandle_Grp',
                                     spaceGrp='Orient_Grp',
                                     stretch=0,
                                     squash=0,
                                     lock_scale=0,
                                     dyn=1,
                                     dyn_ctrl=self.hair_switch.ctrl.pynode,
                                     nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                     nucleus=self.CloakFront0.nucleus.pynode,  # nucleus node to connect with
                                     )

    def connect_mesh(self):
        connectMeshSkincluter.connect_ctl_to_mesh([
            ['cloak02_proxy', [
                'Lf_CloakSpace0_0_Fk_CtlGrp',
                'Lf_CloakSpace1_0_Fk_CtlGrp',
                'Lf_CloakBackSpace0_0_Fk_CtlGrp',
                'Rt_CloakBackSpace0_0_Fk_CtlGrp',

            ]]])

    def dynamic_module(self):
        self.hair_switch = cr.Ctrl(name='cloakDyn', crvShape='stick', des=N.SWITCH,
                                   gimbal=0, color='lightBlue', obj='Head_0_Anim_Jnt', size=10)
        self.hair_switch.zrGrp.pynode.setParent('Anim_Grp')
        pmc.parentConstraint('Head_0_Anim_Jnt', self.hair_switch.zrGrp.pynode, mo=1)
        self.hair_switch.ctrl.lock_hide_attr('tx', 'ty', 'tz', 'rx', 'ry', 'rz', 'sx', 'sy', 'sz', 'v')

    def CloakBack_module(self):
        self.CloakBack0_space_L = fkr.FkRig(name='Lf_CloakBackSpace0',
                                jnt=rt.search_tmp_jnt('Lf_CloakBackSpace0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                skin=1,
                                gimbal=0,
                                parent='Spine_3_Skin_Jnt',
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0)

        self.CloakBack0_L = fkr.FkRig(name='Lf_CloakBack0',
                                jnt=rt.search_tmp_jnt('Lf_CloakBack0'),
                                no_tip=0,
                                crvShape='circle',
                                color='red',
                                space_default= 1,
                                skin=1,
                                gimbal=1,
                                space=['Lf_CloakBackSpace0_0_Skin_Jnt', 'CtlJnt_Grp'],
                                parent='Lf_CloakBackSpace0_0_Skin_Jnt',
                                blend_space=1,
                                animGrp='Anim_Grp',
                                skinGrp='SkinJnt_Grp',
                                utilGrp='CtlJnt_Grp',
                                stillGrp='Still_Grp',
                                ikhGrp='IkHandle_Grp',
                                spaceGrp='Orient_Grp',
                                stretch=0,
                                squash=0,
                                lock_scale=0,
                                dyn=1,
                                dyn_ctrl=self.hair_switch.ctrl.pynode,
                                nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                nucleus=self.CloakFront0.nucleus.pynode,  # nucleus node to connect with

                                     )

        self.CloakBack0_space_R = fkr.FkRig(name='Rt_CloakBackSpace0',
                                      jnt=rt.search_tmp_jnt('Rt_CloakBackSpace0'),
                                      no_tip=0,
                                      crvShape='circle',
                                      color='blue',
                                      skin=1,
                                      gimbal=0,
                                      parent='Spine_3_Skin_Jnt',
                                      animGrp='Anim_Grp',
                                      skinGrp='SkinJnt_Grp',
                                      utilGrp='CtlJnt_Grp',
                                      stillGrp='Still_Grp',
                                      ikhGrp='IkHandle_Grp',
                                      spaceGrp='Orient_Grp',
                                      stretch=0,
                                      squash=0,
                                      lock_scale=0)

        self.CloakBack0_R = fkr.FkRig(name='Rt_CloakBack0',
                                     jnt=rt.search_tmp_jnt('Rt_CloakBack0'),
                                     no_tip=0,
                                     crvShape='circle',
                                     color='blue',
                                     space_default=1,
                                     skin=1,
                                     gimbal=1,
                                     space=['Rt_CloakBackSpace0_0_Skin_Jnt', 'CtlJnt_Grp'],
                                     parent='Rt_CloakBackSpace0_0_Skin_Jnt',
                                     blend_space=1,
                                     animGrp='Anim_Grp',
                                     skinGrp='SkinJnt_Grp',
                                     utilGrp='CtlJnt_Grp',
                                     stillGrp='Still_Grp',
                                     ikhGrp='IkHandle_Grp',
                                     spaceGrp='Orient_Grp',
                                     stretch=0,
                                     squash=0,
                                     lock_scale=0,
                                     dyn=1,
                                     dyn_ctrl=self.hair_switch.ctrl.pynode,
                                     nucleus_ctrl=self.setting_ctrl.ctrl.name,
                                     nucleus=self.CloakFront0.nucleus.pynode,  # nucleus node to connect with
                                     )

    def shoulderCloak_detail_module(self):

        for i in range(5):
            self.shoulderCloak_detail = fkr.FkRig(name='Lf_ShoulderCloakDetail{}'.format(i),
                                    jnt=rt.search_tmp_jnt('Lf_ShoulderCloakDetail{}'.format(i)),
                                    no_tip=0,
                                    crvShape='cube',
                                    color='red',
                                    skin=1,
                                    gimbal=1,
                                    parent='Lf_ShoulderCloakOffset_0_Skin_Jnt',
                                    animGrp='Anim_Grp',
                                    skinGrp='SkinJnt_Grp',
                                    utilGrp='CtlJnt_Grp',
                                    stillGrp='Still_Grp',
                                    ikhGrp='IkHandle_Grp',
                                    spaceGrp='Orient_Grp',
                                    stretch=0,
                                    squash=0,
                                    lock_scale=0)

    def set_dyn_value(self):
        dyn = cmds.ls(type='hairSystem')
        cmds.sets(dyn, n='hairSystem_0_Anim_Set')
        for d in dyn:
            cmds.setAttr("{}.attractionScale[0].attractionScale_Position".format(d), 0.75)
            cmds.setAttr("{}.attractionDamp".format(d), 0.5)

        for ctrl in [self.hair_switch.ctrl.name]:
            for attr in cmds.listAttr(ctrl, ud=1):
                if 'CurveAttract' in attr:
                    cmds.setAttr('{}.{}'.format(ctrl, attr), .75)

