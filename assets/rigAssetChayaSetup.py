from rigpackage.rigasset import rigAssetPreset
reload(rigAssetPreset)
from rigpackage.rigmodule import rigModuleUtil as RigBipedModule
from rigpackage.rigmodule import rigQuadrupedModuleUtil as RigQuadrupedModule
from rigpackage.riglib.quadrupedlib import rigLibQuadrupedUtil as RigQuadrupedLib
from rigpackage.rigmodule.fkmodule import rigTools as rt
reload(rt)
from rigpackage.rigmodule.fkmodule import ctrlRig as cr
reload(cr)
from rigpackage.rigmodule.fkmodule import naming as N
reload(N)
from rigpackage.rigmodule.voljointmodule import volumeJointCreator as vj

reload(vj)


import pymel.core as pmc
import maya.mel as mel
import os
import maya.cmds as cmds
import maya.cmds as mc

class AssetGenericHumanSetup(rigAssetPreset.HumanPreset):

	def __init__(self, *args, **kwargs):

		super(AssetGenericHumanSetup, self).__init__(*args, **kwargs)


class AssetChayaHumanSetup(rigAssetPreset.HumanPreset):
	Eyes_Rig = True
	RibbonLimb_Rig = True
	def __init__(self, *args, **kwargs):

		super(AssetChayaHumanSetup, self).__init__(*args, **kwargs)


	def initialize(self):
		self.human_preset = rigAssetPreset.rigBipedAsset.BipedRig()
	def moduleStructure(self):
		#self.human_preset.fullBodyCtrlRig()
		self.human_preset.rigGrp()
		self.human_preset.placementCtrlRig()
		self.human_preset.bodyCtrlRig()
		self.human_preset.spineCtrlRig()
		self.human_preset.neckCtrlRig(parent= self.human_preset.spineMainJnt_list[-1])
		self.human_preset.headCtrlRig(parent= self.human_preset.neckJnt_list[-1])
		if self.Eyes_Rig:
			self.human_preset.eyesCtrlRig(parent= self.human_preset.headJnt_list[0])

		l_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Lf_')
		r_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Rt_')
		l_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Lf_')
		r_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Rt_')

		l_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Lf_')
		r_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Rt_')
		self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Lf_')
		self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Rt_')

		self.human_preset.nrJnt_arm(side='Lf_', extra='')
		self.human_preset.nrJnt_arm(side='Rt_', extra='')
		self.human_preset.nrJnt_leg(side='Lf_', extra='')
		self.human_preset.nrJnt_leg(side='Rt_', extra='')

		# parent MainGrp into placement ctl
		mc.parent(self.human_preset.anim_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ctlJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.skinJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ikH_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.distance_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.jntTmp_grp, self.human_preset.still_grp)

		# parent spineCtl to Body ctl
		hip_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[0]['oriGrp']
		chest_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[3]['oriGrp']
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[0], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[1], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], hip_ctlIkOriGrp, mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], chest_ctlIkOriGrp, mo=True)

		mc.parent(self.human_preset.spineSkinJnt_list[0], self.human_preset.bodyJntSkin_List[0])
		mc.parent(self.human_preset.neckSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(self.human_preset.headSkinJnt_list[0], self.human_preset.neckSkinJnt_list[-1])
		# mc.parent(self.human_preset.l_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])
		# mc.parent(self.human_preset.r_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])

		mc.parent(l_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(r_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(l_armSkinJnt_list[0], l_shoulderSkinJnt_list[-1])
		mc.parent(r_armSkinJnt_list[0], r_shoulderSkinJnt_list[-1])

		mc.parent(l_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])
		mc.parent(r_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])

		if self.RibbonLimb_Rig:
			for side in'Lf_', 'Rt_':
				self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)
				self.human_preset.ribbonCtrl_leg(side=side, extra='', skinJnt=True)
				cmds.setAttr('{}KneeMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}LegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}ElbowMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}ArmMid_Rib_Ctl.autoTwist'.format(side), 1)

		self.create_setting_ctrl()

	def post_build(self):
		super(AssetChayaHumanSetup, self)
		if self.RibbonLimb_Rig:
			self.add_ctrl_vis()
		self.add_jnt_vis()
		#self.parent_fingerFk_to_skinJnt()
		self.set_joint_hierarchy()
		self.prop_ctrl()

		# if self.rigType == 'deformation':
		#     rt.add_01_attr(self.setting_ctrl.ctrl, 'volumeCtrl')
		#     self.setting_ctrl.ctrl.pynode.attr('volumeCtrl') >> pmc.PyNode(self.vol_jnt).v

	def parent_fingerFk_to_skinJnt(self):
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
			for fin in fingers_list:
				cmds.parent('{}_{}Fin_0_Fk_Jnt'.format(side, fin), '{}_Hand_0_Skin_Jnt'.format(side))


	def set_joint_hierarchy(self):
		# create finger skin joint
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
		  for fin in fingers_list:
			first_fin = '{}_{}Fin_0_Fk_Jnt'.format(side, fin)
			ch_jnt = cmds.listRelatives(first_fin, ad=1, type = 'joint')
			ch_jnt.append(first_fin)
			skn_tmp = []
			for jnt in ch_jnt[::-1]:
			  if 'Pos' not in jnt:
				skn_tmp.append(jnt)
			skin_jnt_list = []

			for jnt in skn_tmp:
			  skn_jnt= cmds.createNode('joint', n= jnt.replace('_Fk_','_Skin_'))
			  cmds.delete(cmds.parentConstraint(jnt, skn_jnt, mo=0))
			  cmds.makeIdentity(skn_jnt, t=0, r=1, s=1, a=1)
			  skin_jnt_list.append(skn_jnt)

			for num in range(len(skin_jnt_list)-1):
			  cmds.parent(skin_jnt_list[num+1], skin_jnt_list[num])
			
			cmds.parent(skin_jnt_list[0], '{}_Hand_0_Skin_Jnt'.format(side))
			
			for fkJnt, sknJnt in zip(skn_tmp, skin_jnt_list):
			  cmds.parentConstraint(fkJnt, sknJnt,mo=1)
		
		# create reference joint
		ref_jnt = cmds.createNode('joint',n='Reference')
		cmds.parent(ref_jnt,'SkinJnt_Grp')
		cmds.parent('Body_0_Skin_Jnt',ref_jnt)
		
		# parent eye joint to head if eye exists
		for side in 'Lf', 'Rt':
			if mc.objExists('{}_Eye_0_Skin_Jnt'.format(side)):
				cmds.parent('{}_Eye_0_Skin_Jnt'.format(side), 'Head_0_Skin_Jnt')

		# create hand prop joint
		handProp_L =cmds.createNode('joint',n = 'Lf_Prop_0_Skin_Jnt')
		handProp_R = cmds.createNode('joint',n = 'Rt_Prop_0_Skin_Jnt')
		# snap hand joint prop
		cmds.delete(cmds.parentConstraint('Lf_Hand_0_Skin_Jnt', handProp_L, mo=0))
		cmds.delete(cmds.parentConstraint('Rt_Hand_0_Skin_Jnt', handProp_R, mo=0))

		# freeze transform
		cmds.makeIdentity(handProp_L,r=1,s=0,a=1,t=0)
		cmds.makeIdentity(handProp_R,r=1,s=0,a=1,t=0)
		# offset hand prop
		pml = cmds.xform(handProp_L,ws=1,q=1,m=1)
		pmr = cmds.xform(handProp_R,ws=1,q=1,m=1)

		cmds.move(pml[8]*3,pml[9]*3,pml[10]*3,handProp_L,r=1)
		cmds.move(-pmr[8]*3,-pmr[9]*3,-pmr[10]*3,handProp_R,r=1)

		cmds.move(5 ,0 ,0 ,handProp_L,os=1, r=1)
		cmds.move(-5,0,0, handProp_R ,os=1 ,r=1)
		# parent prop to hand
		cmds.parent(handProp_L,'Lf_Hand_0_Skin_Jnt')
		cmds.parent(handProp_R,'Rt_Hand_0_Skin_Jnt')
	
	def prop_ctrl(self):
		mc.addAttr(self.setting_ctrl.ctrl.name,ln='propCtrl',k=1,dv=0,max=1,min=0)
		for side,color in zip(['Lf','Rt'],['red','blue']):
			ctrl = cr.Ctrl(name = 'prop',
							pos = side,
							idx=0,
							des = 'Fk',
							crvShape = 'cube',
							size = 4,
							gimbal=1,
							color = color,
							obj = '{}_Prop_0_Skin_Jnt'.format(side))
			mc.parentConstraint(ctrl.ctrl.name,'{}_Prop_0_Skin_Jnt'.format(side),mo=1)

			rt.add_local_world(ctrl=ctrl.ctrl.name,world_obj='Anim_Grp',local_obj='{}_Hand_0_Skin_Jnt'.format(side),cons_grp=ctrl.zrGrp.name,types='parent')

			mc.parent(ctrl.zrGrp.name,'Anim_Grp')
			mc.connectAttr(self.setting_ctrl.ctrl.name+'.propCtrl',ctrl.zrGrp.name+'.v' )



	def create_setting_ctrl(self):
		self.setting_ctrl = cr.Ctrl(name='setting', crvShape='gear', color='yellow', gimbal=0, size=50, des=N.ANIM)
		self.setting_ctrl.zrGrp.pynode.setParent('Anim_Grp')
		for trans in 'trs':
			for ax in 'xyz':
				self.setting_ctrl.ctrl.pynode.attr('{}{}'.format(trans, ax)).set(k=0, l=1)
		self.setting_ctrl.ctrl.pynode.v.set(k=0, l=1)



	def add_ctrl_vis(self):
		parts = {'Arm': ['Arm', 'Elbow'], 'Leg': ['Leg', 'Knee']}
		sides = ['Lf', 'Rt']

		rt.add_lock_attr(self.setting_ctrl.ctrl, 'visibilityCtrl')

		# add topic attr
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'ribbonVis')
		# add limb ribbon vis
		for part in parts.keys():
			for side in sides:
				rt.add_01_attr(self.setting_ctrl.ctrl, '{}{}Ribbon'.format(side, part))
				for sub_part in parts[part]:
					rib_ctrl_grp = pmc.PyNode('{}_{}_RibCtl_Grp'.format(side, sub_part))
					self.setting_ctrl.ctrl.pynode.attr('{}{}Ribbon'.format(side, part)) >> rib_ctrl_grp.v

		for part in ['Arm', 'Leg']:
			for side in sides:
				rib_point_grp = pmc.PyNode('{}_{}_RibPoint_CtlGrp'.format(side, part))
				self.setting_ctrl.ctrl.pynode.attr('{}{}Ribbon'.format(side, part)) >> rib_point_grp.v

		if self.facial:
			self.facial_ctrl_vis()
		# add topic attr
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'detailVis')


	def add_jnt_vis(self):
		# add ctrlJoint/skinJoint vis
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'jntVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'ctrlJointVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'skinJointVis')
		self.setting_ctrl.ctrl.pynode.attr('ctrlJointVis') >> pmc.PyNode('CtlJnt_Grp').v
		self.setting_ctrl.ctrl.pynode.attr('skinJointVis') >> pmc.PyNode('SkinJnt_Grp').v



	def facial_ctrl_vis(self):
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'facialVis')
		# rt.add_01_attr(self.setting_ctrl.ctrl, 'allFacialCtrl')
		# self.setting_ctrl.ctrl.pynode.attr('allFacialCtrl') >> pmc.PyNode('Fcl_0_Ctl_Grp').v

		rt.add_01_attr(self.setting_ctrl.ctrl, 'bshCtrl')
		self.setting_ctrl.ctrl.pynode.attr('bshCtrl') >> pmc.PyNode('BshCtrl_grp').v

		# add macro vis
		macro_list = [u'Rt_CheekIn_0_Fcl_CtlShape',
					  u'Rt_BrowFlash_2_Fcl_CtlShape',
					  u'Lf_Nose_0_Fcl_CtlShape',
					  u'Rt_Nasolabial_1_Fcl_CtlShape',
					  u'Nose_1_Fcl_CtlShape',
					  u'Rt_Nose_0_Fcl_CtlShape',
					  u'Lf_CheekUp_0_Fcl_CtlShape',
					  u'Rt_CheekUp_0_Fcl_CtlShape',
					  u'Nose_0_Fcl_CtlShape',
					  u'Lf_Nasolabial_1_Fcl_CtlShape',
					  u'Nose_2_Fcl_CtlShape',
					  u'Lf_CheekIn_0_Fcl_CtlShape',
					  u'Rt_BrowFlash_3_Fcl_CtlShape',
					  u'Rt_BrowFlash_1_Fcl_CtlShape',
					  u'Rt_BrowFlash_0_Fcl_CtlShape',
					  u'Rt_Brow_1_Fcl_CtlShape',
					  u'Rt_Brow_0_Fcl_CtlShape',
					  u'Lf_Brow_0_Fcl_CtlShape',
					  u'Brow_0_Fcl_CtlShape',
					  u'Lf_Brow_1_Fcl_CtlShape',
					  u'Rt_Brow_2_Fcl_CtlShape',
					  u'Lf_Brow_2_Fcl_CtlShape',
					  u'Lf_Brow_3_Fcl_CtlShape',
					  u'Rt_Brow_3_Fcl_CtlShape',
					  u'Lf_BrowFlash_0_Fcl_CtlShape',
					  u'Lf_BrowFlash_3_Fcl_CtlShape',
					  u'Lf_Ear_0_Fcl_CtlShape',
					  u'Neck_1_Fcl_CtlShape',
					  u'Lf_BrowFlash_1_Fcl_CtlShape',
					  u'Lf_BrowFlash_2_Fcl_CtlShape',
					  u'Lf_Neck_0_Fcl_CtlShape',
					  u'Rt_Neck_0_Fcl_CtlShape',
					  u'Rt_Ear_0_Fcl_CtlShape',
					  u'TopLip_0_Fcl_CtlShape',
					  u'TopMuzzle_0_Fcl_CtlShape',
					  u'Lf_TopLip_0_Fcl_CtlShape',
					  u'Neck_0_Fcl_CtlShape',
					  u'Lf_TopLipOut_0_Fcl_CtlShape',
					  u'Jaw_0_Fcl_CtlShape',
					  u'DoubleChin_0_Fcl_CtlShape',
					  u'BotTeeth_0_Fcl_CtlShape',
					  u'Rt_TopLip_0_Fcl_CtlShape',
					  u'Head_0_Fcl_CtlShape',
					  u'Muzzle_0_Fcl_CtlShape',
					  u'Lf_CornerLip_0_Fcl_CtlShape',
					  u'Rt_TopLipOut_0_Fcl_CtlShape',
					  u'Rt_CornerLip_0_Fcl_CtlShape',
					  u'Tongue_0_Rvs_CtlShape',
					  u'BotTeeth_0_Rvs_CtlShape',
					  u'Lf_Cheek_0_Fcl_CtlShape',
					  u'Chin_0_Fcl_CtlShape',
					  u'Rt_Cheek_0_Fcl_CtlShape',
					  u'TopTeeth_0_Fcl_CtlShape',
					  u'BotMuzzle_0_Fcl_CtlShape',
					  u'Rt_BotLip_0_Fcl_CtlShape',
					  u'Lf_MouthCorner_0_Fcl_CtlShape',
					  u'BotLip_0_Fcl_CtlShape',
					  u'Lf_BotLip_0_Fcl_CtlShape',
					  u'Lf_BotLipOut_0_Fcl_CtlShape',
					  u'Rt_BotLipOut_0_Fcl_CtlShape',
					  u'Lf_Nasolabial_0_Fcl_CtlShape',
					  u'Rt_MouthCorner_0_Fcl_CtlShape',
					  u'Rt_Nasolabial_0_Fcl_CtlShape',
					  u'Lf_CenterEye_0_Fcl_CtlShape',
					  u'Lf_TopLid_0_Fcl_CtlShape',
					  u'Lf_BaseEye_0_Fcl_CtlShape',
					  u'Lf_Eyeball_0_Fcl_CtlShape',
					  u'Lf_BotLid_0_Fcl_CtlShape',
					  u'Rt_CenterEye_0_Fcl_CtlShape',
					  u'Rt_TopLid1_0_Fcl_CtlShape',
					  u'Rt_BotLid_0_Fcl_CtlShape',
					  u'Lf_CornerLidIn_0_Fcl_CtlShape',
					  u'Lf_TopLid2_0_Fcl_CtlShape',
					  u'Lf_TopLid1_0_Fcl_CtlShape',
					  u'Lf_BotLid1_0_Fcl_CtlShape',
					  u'Lf_BotLid2_0_Fcl_CtlShape',
					  u'Lf_CornerLidOut_0_Fcl_CtlShape',
					  u'Rt_BaseEye_0_Fcl_CtlShape',
					  u'Rt_Eyeball_0_Fcl_CtlShape',
					  u'Rt_TopLid_0_Fcl_CtlShape',
					  u'Rt_TopLid2_0_Fcl_CtlShape',
					  u'Rt_BotLid1_0_Fcl_CtlShape',
					  u'Rt_BotLid2_0_Fcl_CtlShape',
					  u'Rt_CornerLidIn_0_Fcl_CtlShape',
					  u'Rt_CornerLidOut_0_Fcl_CtlShape']
		rt.add_01_attr(self.setting_ctrl.ctrl, 'macroCtrl')
		for ctrl in macro_list:
			cmds.connectAttr('Setting_0_Anim_Ctl.macroCtrl', ctrl + '.v')

		# add micro vis
		micro_list = [u'Rt_NoseUp_0_Fcl_CtlShape',
					  u'NoseUp_0_Fcl_CtlShape',
					  u'Lf_NoseUp_0_Fcl_CtlShape',
					  u'BrowFlash_0_Fcl_CtlShape',
					  u'Forehead_0_Fcl_CtlShape',
					  u'Lf_Forehead_0_Fcl_CtlShape',
					  u'Lf_Forehead_1_Fcl_CtlShape',
					  u'Lf_Forehead_2_Fcl_CtlShape',
					  u'Lf_Forehead_3_Fcl_CtlShape',
					  u'Lf_Temple_0_Fcl_CtlShape',
					  u'Rt_Forehead_0_Fcl_CtlShape',
					  u'Rt_Forehead_1_Fcl_CtlShape',
					  u'Rt_Forehead_2_Fcl_CtlShape',
					  u'Rt_Forehead_3_Fcl_CtlShape',
					  u'Rt_Temple_0_Fcl_CtlShape',
					  u'Rt_BrowOfs_3_Fcl_CtlShape',
					  u'Rt_BrowOfs_2_Fcl_CtlShape',
					  u'Rt_BrowOfs_1_Fcl_CtlShape',
					  u'Rt_BrowOfs_0_Fcl_CtlShape',
					  u'Lf_BrowOfs_0_Fcl_CtlShape',
					  u'Lf_BrowOfs_1_Fcl_CtlShape',
					  u'Lf_BrowOfs_2_Fcl_CtlShape',
					  u'Lf_BrowOfs_3_Fcl_CtlShape',
					  u'Lf_BotLid2_1_Fcl_CtlShape',
					  u'Lf_CornerLidOut_1_Fcl_CtlShape',
					  u'Lf_TopLid2_1_Fcl_CtlShape',
					  u'Lf_TopLid1_1_Fcl_CtlShape',
					  u'Lf_CornerLidIn_1_Fcl_CtlShape',
					  u'Lf_BotLid1_1_Fcl_CtlShape',
					  u'Lf_Eyebags_0_Fcl_CtlShape',
					  u'Lf_Eyebags_1_Fcl_CtlShape',
					  u'Rt_TopLid1_1_Fcl_CtlShape',
					  u'Rt_TopLid2_1_Fcl_CtlShape',
					  u'Rt_CornerLidOut_1_Fcl_CtlShape',
					  u'Rt_BotLid2_1_Fcl_CtlShape',
					  u'Rt_Eyebags_1_Fcl_CtlShape',
					  u'Rt_BotLid1_1_Fcl_CtlShape',
					  u'Rt_Eyebags_0_Fcl_CtlShape',
					  u'Rt_CornerLidIn_1_Fcl_CtlShape',
					  u'Rt_Nose_1_Fcl_CtlShape',
					  u'Lf_Nose_1_Fcl_CtlShape',
					  u'Nose_3_Fcl_CtlShape',
					  u'TopLip_1_Fcl_CtlShape',
					  u'Lf_TopLip_1_Fcl_CtlShape',
					  u'Lf_TopLipOut_1_Fcl_CtlShape',
					  u'Lf_BotLip_1_Fcl_CtlShape',
					  u'Lf_BotLipOut_1_Fcl_CtlShape',
					  u'Lf_CornerLip_1_Fcl_CtlShape',
					  u'BotLip_1_Fcl_CtlShape',
					  u'Rt_BotLip_1_Fcl_CtlShape',
					  u'Rt_TopLip_1_Fcl_CtlShape',
					  u'Rt_TopLipOut_1_Fcl_CtlShape',
					  u'Rt_BotLipOut_1_Fcl_CtlShape',
					  u'Rt_CornerLip_1_Fcl_CtlShape',
					  u'Rt_Chin_0_Fcl_CtlShape',
					  u'Chin_1_Fcl_CtlShape',
					  u'Lf_Chin_0_Fcl_CtlShape',
					  u'TopHead_0_Fcl_CtlShape',
					  u'BotHead_0_Fcl_CtlShape',
					  # u'Lf_Pupil_0_Fcl_CtlShape',
					  # u'Lf_Iris_0_Fcl_CtlShape',
					  # u'Rt_Pupil_0_Fcl_CtlShape',
					  # u'Rt_Iris_0_Fcl_CtlShape',
					  u'Rt_BotTeeth_Fcl_CtlShape',
					  u'Lf_BotTeeth_Fcl_CtlShape',
					  u'Lf_TopTeeth_0_Fcl_CtlShape',
					  u'Rt_TopTeeth_0_Fcl_CtlShape']
		rt.add_01_attr(self.setting_ctrl.ctrl, 'microCtrl')
		for ctrl in micro_list:
			cmds.connectAttr('Setting_0_Anim_Ctl.microCtrl', ctrl + '.v')

		tongue_list = [u'Tongue_5_Fcl_CtlShape',
					   u'Tongue_6_Fcl_CtlShape',
					   u'Tongue_4_Fcl_CtlShape',
					   u'Tongue_3_Fcl_CtlShape',
					   u'Tongue_2_Fcl_CtlShape',
					   u'Tongue_1_Fcl_CtlShape',
					   u'Tongue_0_Fcl_CtlShape']
		rt.add_01_attr(self.setting_ctrl.ctrl, 'tongueCtrl')
		for ctrl in tongue_list:
			cmds.connectAttr('Setting_0_Anim_Ctl.tongueCtrl', ctrl + '.v')

		eyelash_list = ['Rt_EyeLash_0_FclCtl_Grp', 'Lf_EyeLash_0_FclCtl_Grp']
		rt.add_01_attr(self.setting_ctrl.ctrl, 'eyelashCtrl')
		for ctrl in eyelash_list:
			cmds.connectAttr('Setting_0_Anim_Ctl.eyelashCtrl', ctrl + '.v')

	def vol_jnt_module(self):
		self.vol_jnt = vj.create_volume_joint(body =0 , neck =0, head=0, clavicle = 1,
						arm =1 , elbow =1, wrist = 1,
						leg =0 , knee =1, ankle=0)
		# vol_jnt_list = [u'Spine1_0_Vol_ModuleGrp',
		#                 u'Spine2_0_Vol_ModuleGrp',
		#                 u'Spine3_0_Vol_ModuleGrp',
		#                 u'Neck_0_Vol_ModuleGrp',
		#                 u'Head_0_Vol_ModuleGrp',
		#                 u'Lf_Clavicle_0_Vol_ModuleGrp',
		#                 u'Rt_Clavicle_0_Vol_ModuleGrp',
		#                 u'Lf_Arm_0_Vol_ModuleGrp',
		#                 u'Rt_Arm_0_Vol_ModuleGrp',
		#                 u'Lf_Elbow_0_Vol_ModuleGrp',
		#                 u'Rt_Elbow_0_Vol_ModuleGrp',
		#                 u'Lf_Wrist_0_Vol_ModuleGrp',
		#                 u'Rt_Wrist_0_Vol_ModuleGrp',
		#                 u'Lf_Leg_0_Vol_ModuleGrp',
		#                 u'Rt_Leg_0_Vol_ModuleGrp',
		#                 u'Lf_Knee_0_Vol_ModuleGrp',
		#                 u'Rt_Knee_0_Vol_ModuleGrp',
		#                 u'Lf_Ankle_0_Vol_ModuleGrp',
		#                 u'Rt_Ankle_0_Vol_ModuleGrp']

		vol_jnt_list = [
						u'Lf_Clavicle_0_Vol_ModuleGrp',
						u'Rt_Clavicle_0_Vol_ModuleGrp',
						u'Lf_Arm_0_Vol_ModuleGrp',
						u'Rt_Arm_0_Vol_ModuleGrp',
						u'Lf_Elbow_0_Vol_ModuleGrp',
						u'Rt_Elbow_0_Vol_ModuleGrp',
						u'Lf_Wrist_0_Vol_ModuleGrp',
						u'Rt_Wrist_0_Vol_ModuleGrp',
						u'Lf_Knee_0_Vol_ModuleGrp',
						u'Rt_Knee_0_Vol_ModuleGrp',]

		cmds.parent(vol_jnt_list, 'VolJnt_Grp')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'volumeCtrl')
		self.setting_ctrl.ctrl.pynode.attr('volumeCtrl') >> pmc.PyNode(self.vol_jnt).v




class AssetChayaFaunSetup(rigAssetPreset.HumanPreset):
	#Eyes_Rig = True
	def __init__(self, *args, **kwargs):
		super(AssetChayaFaunSetup, self).__init__(*args, **kwargs)

	def initialize(self):


		self.human_preset = rigAssetPreset.rigBipedAsset.BipedRig()
		self.quadruped_preset = rigAssetPreset.rigQuadrupedAsset.QuadrupedRig()

		self.human_preset.rigGrp()
		self.human_preset.placementCtrlRig()
		self.human_preset.bodyCtrlRig()
		self.human_preset.spineCtrlRig()
		self.human_preset.neckCtrlRig(parent=self.human_preset.spineMainJnt_list[-1])
		self.human_preset.headCtrlRig(parent=self.human_preset.neckJnt_list[-1])
		if self.Eyes_Rig:
			self.human_preset.eyesCtrlRig(parent= self.human_preset.headJnt_list[0])
		l_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Lf_')
		r_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Rt_')
		l_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Lf_')
		r_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Rt_')

		# l_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Lf_')
		# r_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Rt_')
		self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Lf_')
		self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Rt_')

		self.human_preset.nrJnt_arm(side='Lf_', extra='')
		self.human_preset.nrJnt_arm(side='Rt_', extra='')
		# self.human_preset.nrJnt_leg(side='Lf_', extra='')
		# self.human_preset.nrJnt_leg(side='Rt_', extra='')

		# parent MainGrp into placement ctl
		mc.parent(self.human_preset.anim_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ctlJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.skinJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ikH_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.distance_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.jntTmp_grp, self.human_preset.still_grp)

		# parent spineCtl to Body ctl
		hip_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[0]['oriGrp']
		chest_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[3]['oriGrp']
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[0], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[1], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], hip_ctlIkOriGrp, mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], chest_ctlIkOriGrp, mo=True)

		mc.parent(self.human_preset.spineSkinJnt_list[0], self.human_preset.bodyJntSkin_List[0])
		mc.parent(self.human_preset.neckSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(self.human_preset.headSkinJnt_list[0], self.human_preset.neckSkinJnt_list[-1])
		# mc.parent(self.human_preset.l_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])
		# mc.parent(self.human_preset.r_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])

		mc.parent(l_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(r_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(l_armSkinJnt_list[0], l_shoulderSkinJnt_list[-1])
		mc.parent(r_armSkinJnt_list[0], r_shoulderSkinJnt_list[-1])

		# mc.parent(l_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])
		# mc.parent(r_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])


		#Add quadrupedRig leg module
		for side in 'Lf_', 'Rt_':
			self.quadruped_preset.legCtlRig(parent='Hip_0_Anim_Jnt', side=side, part='')
		
		if self.RibbonLimb_Rig:
			self.nonRoll_joint()
			self.ribbonCtrl_leg()
			for side in 'Lf_','Rt_':
				self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)
				cmds.setAttr('{}UpLegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}MidLegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}LowLegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}ElbowMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}ArmMid_Rib_Ctl.autoTwist'.format(side), 1)
		
		self.create_setting_ctrl()


	def moduleStructure(self):
		# self.human_preset.fullBodyCtrlRig()
		super(AssetChayaFaunSetup, self)
		# for side in'Lf_', 'Rt_':
		#     self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)

	def post_build(self):
		super(AssetChayaFaunSetup, self)
		if self.RibbonLimb_Rig:
			self.add_ctrl_vis()
		self.add_jnt_vis()
		#self.parent_fingerFk_to_skinJnt()
		self.set_joint_hierarchy()
		self.prop_ctrl()

		# if self.rigType == 'deformation':
		#     rt.add_01_attr(self.setting_ctrl.ctrl, 'volumeCtrl')
		#     self.setting_ctrl.ctrl.pynode.attr('volumeCtrl') >> pmc.PyNode(self.vol_jnt).v

	def parent_fingerFk_to_skinJnt(self):
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
			for fin in fingers_list:
				cmds.parent('{}_{}Fin_0_Fk_Jnt'.format(side, fin), '{}_Hand_0_Skin_Jnt'.format(side))
	

	def nonRoll_joint(self):
		for i in 'Lf_', 'Rt_':
			self.nrJnt_leg(i,extra='')
			self.nrJnt_extraLeg(i,extra='')

	def nrJnt_extraLeg(self,side,extra):

		mc.select('%sFeet%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sToe%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legExtraNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='')


	def nrJnt_leg(self,side,extra):
		mc.select('%sUpLeg%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='Hip_0_Anim_Jnt')

	def ribbonCtrl_leg(self, side='l', extra='', skinJnt=True):

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra))
			mc.select('%sUpLeg%s_0_NrTw_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Up', part='UpLeg%s' % extra, skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sLowLeg%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='MidLeg%s' % extra,
														skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%sMid_Rib_Ctl' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			mc.select('%sLowMidLeg%sTop_RibPosi_Loc' % (side, extra), tgl=True)
			mc.select('%sUpUpLeg%sLow_RibPosi_Loc' % (side, extra), tgl=True)
			RigBipedModule.Ribbon.ribbonPointCtl(side=side, part='Leg%s' % extra, shape='sphere')

		for side in 'Lf_', 'Rt_':
			mc.select('%sFeet%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='LowLeg%s' % extra,
														skinJnt=skinJnt)


	def set_joint_hierarchy(self):
		# create finger skin joint
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
		  for fin in fingers_list:
			first_fin = '{}_{}Fin_0_Fk_Jnt'.format(side, fin)
			ch_jnt = cmds.listRelatives(first_fin, ad=1, type = 'joint')
			ch_jnt.append(first_fin)
			skn_tmp = []
			for jnt in ch_jnt[::-1]:
			  if 'Pos' not in jnt:
				skn_tmp.append(jnt)
			skin_jnt_list = []

			for jnt in skn_tmp:
			  skn_jnt= cmds.createNode('joint', n= jnt.replace('_Fk_','_Skin_'))
			  cmds.delete(cmds.parentConstraint(jnt, skn_jnt, mo=0))
			  cmds.makeIdentity(skn_jnt, t=0, r=1, s=1, a=1)
			  skin_jnt_list.append(skn_jnt)

			for num in range(len(skin_jnt_list)-1):
			  cmds.parent(skin_jnt_list[num+1], skin_jnt_list[num])
			
			cmds.parent(skin_jnt_list[0], '{}_Hand_0_Skin_Jnt'.format(side))
			
			for fkJnt, sknJnt in zip(skn_tmp, skin_jnt_list):
			  cmds.parentConstraint(fkJnt, sknJnt,mo=1)
		
		# create reference joint
		ref_jnt = cmds.createNode('joint',n='Reference')
		cmds.parent(ref_jnt,'SkinJnt_Grp')
		cmds.parent('Body_0_Skin_Jnt',ref_jnt)
		
		# parent eye joint to head if eye exists
		for side in 'Lf', 'Rt':
			if mc.objExists('{}_Eye_0_Skin_Jnt'.format(side)):
				cmds.parent('{}_Eye_0_Skin_Jnt'.format(side), 'Head_0_Skin_Jnt')

		# create hand prop joint
		handProp_L =cmds.createNode('joint',n = 'Lf_Prop_0_Skin_Jnt')
		handProp_R = cmds.createNode('joint',n = 'Rt_Prop_0_Skin_Jnt')
		# snap hand joint prop
		cmds.delete(cmds.parentConstraint('Lf_Hand_0_Skin_Jnt', handProp_L, mo=0))
		cmds.delete(cmds.parentConstraint('Rt_Hand_0_Skin_Jnt', handProp_R, mo=0))

		# freeze transform
		cmds.makeIdentity(handProp_L,r=1,s=0,a=1,t=0)
		cmds.makeIdentity(handProp_R,r=1,s=0,a=1,t=0)
		# offset hand prop
		pml = cmds.xform(handProp_L,ws=1,q=1,m=1)
		pmr = cmds.xform(handProp_R,ws=1,q=1,m=1)

		cmds.move(pml[8]*3,pml[9]*3,pml[10]*3,handProp_L,r=1)
		cmds.move(-pmr[8]*3,-pmr[9]*3,-pmr[10]*3,handProp_R,r=1)

		cmds.move(5 ,0 ,0 ,handProp_L,os=1, r=1)
		cmds.move(-5,0,0, handProp_R ,os=1 ,r=1)
		# parent prop to hand
		cmds.parent(handProp_L,'Lf_Hand_0_Skin_Jnt')
		cmds.parent(handProp_R,'Rt_Hand_0_Skin_Jnt')
	
	def prop_ctrl(self):
		mc.addAttr(self.setting_ctrl.ctrl.name,ln='propCtrl',k=1,dv=0,max=1,min=0)
		for side,color in zip(['Lf','Rt'],['red','blue']):
			ctrl = cr.Ctrl(name = 'prop',
							pos = side,
							idx=0,
							des = 'Fk',
							crvShape = 'cube',
							size = 4,
							gimbal=1,
							color = color,
							obj = '{}_Prop_0_Skin_Jnt'.format(side))
			mc.parentConstraint(ctrl.ctrl.name,'{}_Prop_0_Skin_Jnt'.format(side),mo=1)

			rt.add_local_world(ctrl=ctrl.ctrl.name,world_obj='Anim_Grp',local_obj='{}_Hand_0_Skin_Jnt'.format(side),cons_grp=ctrl.zrGrp.name,types='parent')

			mc.parent(ctrl.zrGrp.name,'Anim_Grp')
			mc.connectAttr(self.setting_ctrl.ctrl.name+'.propCtrl',ctrl.zrGrp.name+'.v' )



	def create_setting_ctrl(self):
		self.setting_ctrl = cr.Ctrl(name='setting', crvShape='gear', color='yellow', gimbal=0, size=50, des=N.ANIM)
		self.setting_ctrl.zrGrp.pynode.setParent('Anim_Grp')
		for trans in 'trs':
			for ax in 'xyz':
				self.setting_ctrl.ctrl.pynode.attr('{}{}'.format(trans, ax)).set(k=0, l=1)
		self.setting_ctrl.ctrl.pynode.v.set(k=0, l=1)



	def add_ctrl_vis(self):
		parts = {'Arm': ['Arm', 'Elbow'], 'Leg': ['UpLeg', 'MidLeg','LowLeg']}
		sides = ['Lf', 'Rt']

		rt.add_lock_attr(self.setting_ctrl.ctrl, 'visibilityCtrl')

		# add topic attr
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'ribbonVis')
		# add limb ribbon vis
		for part in parts.keys():
			for side in sides:
				rt.add_01_attr(self.setting_ctrl.ctrl, '{}{}Ribbon'.format(side, part))
				for sub_part in parts[part]:
					rib_ctrl_grp = pmc.PyNode('{}_{}_RibCtl_Grp'.format(side, sub_part))
					self.setting_ctrl.ctrl.pynode.attr('{}{}Ribbon'.format(side, part)) >> rib_ctrl_grp.v

		for part in ['Arm', 'Leg']:
			for side in sides:
				rib_point_grp = pmc.PyNode('{}_{}_RibPoint_CtlGrp'.format(side, part))
				self.setting_ctrl.ctrl.pynode.attr('{}{}Ribbon'.format(side, part)) >> rib_point_grp.v

	def add_jnt_vis(self):
		# add ctrlJoint/skinJoint vis
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'jntVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'ctrlJointVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'skinJointVis')
		self.setting_ctrl.ctrl.pynode.attr('ctrlJointVis') >> pmc.PyNode('CtlJnt_Grp').v
		self.setting_ctrl.ctrl.pynode.attr('skinJointVis') >> pmc.PyNode('SkinJnt_Grp').v

class AssetChayaCreatureSetup(rigAssetPreset.HumanPreset):

	def __init__(self, *args, **kwargs):
		super(AssetChayaCreatureSetup, self).__init__(*args, **kwargs)

	def nrJnt_extraLeg(self,side,extra):

		mc.select('%sFeet%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sToe%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legExtraNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='')


	def nrJnt_leg(self,side,extra):
		mc.select('%sUpLeg%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='Hip_0_Anim_Jnt')


	def ribbonCtrl_leg(self, side='l', extra='', skinJnt=True):

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra))
			mc.select('%sUpLeg%s_0_NrTw_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Up', part='UpLeg%s' % extra, skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sLowLeg%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='MidLeg%s' % extra,
														skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%sMid_Rib_Ctl' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			mc.select('%sLowMidLeg%sTop_RibPosi_Loc' % (side, extra), tgl=True)
			mc.select('%sUpUpLeg%sLow_RibPosi_Loc' % (side, extra), tgl=True)
			RigBipedModule.Ribbon.ribbonPointCtl(side=side, part='Leg%s' % extra, shape='sphere')

		for side in 'Lf_', 'Rt_':
			mc.select('%sFeet%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='LowLeg%s' % extra,
														skinJnt=skinJnt)

	def toesCtls_rig(self):
		self.quadruped_preset.toeCtrlRig(parent='Feet_0_Anim_Jnt', extra='', side='Lf_')
		self.quadruped_preset.toeCtrlRig(parent='Feet_0_Anim_Jnt', extra='', side='Rt_')

	def initialize(self):
		self.human_preset = rigAssetPreset.rigBipedAsset.BipedRig()
		self.quadruped_preset = rigAssetPreset.rigQuadrupedAsset.QuadrupedRig()

		self.human_preset.rigGrp()
		self.human_preset.placementCtrlRig()
		self.human_preset.bodyCtrlRig()
		self.human_preset.spineCtrlRig()
		self.human_preset.neckCtrlRig(parent=self.human_preset.spineMainJnt_list[-1])
		self.human_preset.headCtrlRig(parent=self.human_preset.neckJnt_list[-1])
		self.human_preset.eyesCtrlRig(parent=self.human_preset.headJnt_list[0])
		l_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Lf_')
		r_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Rt_')

		# l_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Lf_')
		# r_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Rt_')

		l_armSkinJnt_list = self.quadruped_preset.armFaunCtlRig(parent='Shoulder_0_Anim_Jnt', side='Lf_', part='', extra='')
		r_armSkinJnt_list = self.quadruped_preset.armFaunCtlRig(parent='Shoulder_0_Anim_Jnt', side='Rt_', part='', extra='')

		# self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Lf_')
		# self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Rt_')
		self.quadruped_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Lf_')
		self.quadruped_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Rt_')


		# self.human_preset.nrJnt_arm(side='Lf_', extra='')
		# self.human_preset.nrJnt_arm(side='Rt_', extra='')
		# self.human_preset.nrJnt_leg(side='Lf_', extra='')
		# self.human_preset.nrJnt_leg(side='Rt_', extra='')

		# parent MainGrp into placement ctl
		mc.parent(self.human_preset.anim_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ctlJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.skinJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ikH_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.distance_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.jntTmp_grp, self.human_preset.still_grp)

		# parent spineCtl to Body ctl
		hip_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[0]['oriGrp']
		chest_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[3]['oriGrp']
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[0], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[1], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], hip_ctlIkOriGrp, mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], chest_ctlIkOriGrp, mo=True)

		mc.parent(self.human_preset.spineSkinJnt_list[0], self.human_preset.bodyJntSkin_List[0])
		mc.parent(self.human_preset.neckSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(self.human_preset.headSkinJnt_list[0], self.human_preset.neckSkinJnt_list[-1])
		# mc.parent(self.human_preset.l_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])
		# mc.parent(self.human_preset.r_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])

		mc.parent(l_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(r_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(l_armSkinJnt_list[0], l_shoulderSkinJnt_list[-1])
		mc.parent(r_armSkinJnt_list[0], r_shoulderSkinJnt_list[-1])

		# mc.parent(l_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])
		# mc.parent(r_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])


		#Add quadrupedRig leg module
		for side in 'Lf_', 'Rt_':
			self.quadruped_preset.legCtlRig(parent='Hip_0_Anim_Jnt', side=side, part='')



	def moduleStructure(self):
		# self.human_preset.fullBodyCtrlRig()
		super(AssetChayaCreatureSetup, self)
		# for side in'Lf_', 'Rt_':
		#     self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)

	def post_build(self):
		super(AssetChayaCreatureSetup, self)

		mc.setAttr('{}.localWorld'.format(self.human_preset.headCtlDict['ctl'][0]), 1)

		self.setAttr_ctlsIk()
		# self.setAttr_ctlsRibbon()
		# self.plugRibbon_to_mainJnt()
		# self.ribbon_global_scale_fixed()

	def setAttr_ctlsIk(self):
		for side in 'Lf', 'Rt':
			mc.setAttr('{}_Hand_0_Ik_Ctl.world'.format(side), 1)
			mc.setAttr('{}_Elbow_0_Ik_Ctl.world'.format(side), 1)
			mc.setAttr('{}_Feet_0_Ik_Ctl.world'.format(side), 1)
			mc.setAttr('{}_Knee_0_Ik_Ctl.world'.format(side), 1)

	def setAttr_ctlsRibbon(self):

		for side in 'Lf', 'Rt':
			# Ribbon Auto switch enable
			mc.setAttr('{}_UpArmMid_Rib_Ctl.autoTwist'.format(side), 1)
			mc.setAttr('{}_MidArmMid_Rib_Ctl.autoTwist'.format(side), 1)
			mc.setAttr('{}_LowArmMid_Rib_Ctl.autoTwist'.format(side), 1)

			mc.setAttr('{}_UpLegMid_Rib_Ctl.autoTwist'.format(side), 1)
			mc.setAttr('{}_MidLegMid_Rib_Ctl.autoTwist'.format(side), 1)
			mc.setAttr('{}_LowLegMid_Rib_Ctl.autoTwist'.format(side), 1)

			# Neck
		mc.setAttr('NeckMid_Rib_Ctl.autoTwist'.format(side), 1)

	def plugRibbon_to_mainJnt(self):
		upArm_top_ribbon = 'UpUpArmTop_RibPosi_Loc'
		upArm_low_ribbon = 'UpUpArmLow_RibPosi_Loc'

		midArm_top_ribbon = 'LowMidArmTop_RibPosi_Loc'
		midArm_low_ribbon = 'LowMidArmLow_RibPosi_Loc'

		lowArm_up_ribbon = 'LowLowArmTop_RibPosi_Loc'
		lowArm_low_ribbon = 'LowLowArmLow_RibPosi_Loc'

		upLeg_top_ribbon = 'UpUpLegTop_RibPosi_Loc'
		upLeg_low_ribbon = 'UpUpLegLow_RibPosi_Loc'

		midLeg_top_ribbon = 'LowMidLegTop_RibPosi_Loc'
		midLeg_low_ribbon = 'LowMidLegLow_RibPosi_Loc'

		lowLeg_up_ribbon = 'LowLowLegTop_RibPosi_Loc'
		lowLeg_low_ribbon = 'LowLowLegLow_RibPosi_Loc'

		neck_up_ribbon = 'LowNeckLow_RibPosi_Loc'
		neck_low_ribbon = 'LowNeckTop_RibPosi_Loc'


		for side in 'Lf', 'Rt':
			#Plug Arms
			mc.pointConstraint('{}_UpArm_0_Anim_Jnt'.format(side), '{}_{}'.format(side, upArm_top_ribbon))
			mc.pointConstraint('{}_MidArm_0_Anim_Jnt'.format(side), '{}_{}'.format(side, upArm_low_ribbon))

			mc.pointConstraint('{}_MidArm_0_Anim_Jnt'.format(side), '{}_{}'.format(side, midArm_top_ribbon))
			mc.pointConstraint('{}_LowArm_0_Anim_Jnt'.format(side), '{}_{}'.format(side, midArm_low_ribbon))

			mc.pointConstraint('{}_LowArm_0_Anim_Jnt'.format(side), '{}_{}'.format(side, lowArm_up_ribbon))
			mc.pointConstraint('{}_Hand_0_Anim_Jnt'.format(side), '{}_{}'.format(side, lowArm_low_ribbon))

			#Plug Legs
			mc.pointConstraint('{}_UpLeg_0_Anim_Jnt'.format(side), '{}_{}'.format(side, upLeg_top_ribbon))
			mc.pointConstraint('{}_MidLeg_0_Anim_Jnt'.format(side), '{}_{}'.format(side, upLeg_low_ribbon))

			mc.pointConstraint('{}_MidLeg_0_Anim_Jnt'.format(side), '{}_{}'.format(side, midLeg_top_ribbon))
			mc.pointConstraint('{}_LowLeg_0_Anim_Jnt'.format(side), '{}_{}'.format(side, midLeg_low_ribbon))

			mc.pointConstraint('{}_LowLeg_0_Anim_Jnt'.format(side), '{}_{}'.format(side, lowLeg_up_ribbon))
			mc.pointConstraint('{}_Feet_0_Anim_Jnt'.format(side), '{}_{}'.format(side, lowLeg_low_ribbon))

		mc.pointConstraint('Head_0_Anim_Jnt', '{}'.format(neck_up_ribbon))
		mc.pointConstraint('Neck_0_Anim_Jnt', '{}'.format(neck_low_ribbon))

	def ribbon_global_scale_fixed(self):

		ribbon_list = ['Arm', 'Leg']
		parts = ['Up', 'Mid', 'Low']
		for side in 'Lf', 'Rt':
			for i in range(len(ribbon_list)):
				for part in parts:
					nurb = '{}_{}{}Rib_Nurb'.format(side, part, ribbon_list[i])

					mdv = mc.createNode('multiplyDivide', n='{}_{}{}Rib_globalScale_Mdv'.format(side, part, ribbon_list[i]))
					value = mc.getAttr('{}.divider'.format(nurb))
					mc.setAttr('{}.input2X'.format(mdv), value)

					mc.connectAttr('{}.sx'.format(self.human_preset.placement_ctrl[0]), '{}.input1X'.format(mdv))
					mc.connectAttr('{}.outputX'.format(mdv), '{}.divider'.format(nurb))

		extra_rib_list = ['Neck']
		for ribbon_extra in extra_rib_list:
			nurb = '{}Rib_Nurb'.format(ribbon_extra)
			mdv = mc.createNode('multiplyDivide',
								n='{}Rib_globalScale_Mdv'.format(ribbon_extra))
			value = mc.getAttr('{}.divider'.format(nurb))
			mc.setAttr('{}.input2X'.format(mdv), value)

			mc.connectAttr('{}.sx'.format(self.human_preset.placement_ctrl[0]), '{}.input1X'.format(mdv))
			mc.connectAttr('{}.outputX'.format(mdv), '{}.divider'.format(nurb))

class AssetChayaBirdSetup(rigAssetPreset.HumanPreset):
	#Eyes_Rig = True
	def __init__(self, *args, **kwargs):
		super(AssetChayaBirdSetup, self).__init__(*args, **kwargs)

	def initialize(self):


		self.human_preset = rigAssetPreset.rigBipedAsset.BipedRig()
		self.quadruped_preset = rigAssetPreset.rigQuadrupedAsset.QuadrupedRig()

		self.human_preset.rigGrp()
		self.human_preset.placementCtrlRig()
		self.human_preset.bodyCtrlRig()
		self.human_preset.spineCtrlRig()
		self.human_preset.neckCtrlRig(parent=self.human_preset.spineMainJnt_list[-1])
		self.human_preset.headCtrlRig(parent=self.human_preset.neckJnt_list[-1])
		if self.Eyes_Rig:
			self.human_preset.eyesCtrlRig(parent= self.human_preset.headJnt_list[0])
		l_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Lf_')
		r_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Rt_')
		l_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Lf_')
		r_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Rt_')

		# l_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Lf_')
		# r_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Rt_')
		# self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Lf_')
		# self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Rt_')

		self.human_preset.nrJnt_arm(side='Lf_', extra='')
		self.human_preset.nrJnt_arm(side='Rt_', extra='')
		# self.human_preset.nrJnt_leg(side='Lf_', extra='')
		# self.human_preset.nrJnt_leg(side='Rt_', extra='')

		# parent MainGrp into placement ctl
		mc.parent(self.human_preset.anim_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ctlJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.skinJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ikH_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.distance_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.jntTmp_grp, self.human_preset.still_grp)

		# parent spineCtl to Body ctl
		hip_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[0]['oriGrp']
		chest_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[3]['oriGrp']
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[0], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[1], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], hip_ctlIkOriGrp, mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], chest_ctlIkOriGrp, mo=True)

		mc.parent(self.human_preset.spineSkinJnt_list[0], self.human_preset.bodyJntSkin_List[0])
		mc.parent(self.human_preset.neckSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(self.human_preset.headSkinJnt_list[0], self.human_preset.neckSkinJnt_list[-1])
		# mc.parent(self.human_preset.l_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])
		# mc.parent(self.human_preset.r_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])

		mc.parent(l_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(r_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(l_armSkinJnt_list[0], l_shoulderSkinJnt_list[-1])
		mc.parent(r_armSkinJnt_list[0], r_shoulderSkinJnt_list[-1])

		# mc.parent(l_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])
		# mc.parent(r_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])


		#Add quadrupedRig leg module
		for side in 'Lf_', 'Rt_':
			self.quadruped_preset.legCtlRig(parent='Hip_0_Anim_Jnt', side=side, part='')
		
		if self.RibbonLimb_Rig:
			self.nonRoll_joint()
			self.ribbonCtrl_leg()
			for side in 'Lf_','Rt_':
				self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)
				cmds.setAttr('{}UpLegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}MidLegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}LowLegMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}ElbowMid_Rib_Ctl.autoTwist'.format(side), 1)
				cmds.setAttr('{}ArmMid_Rib_Ctl.autoTwist'.format(side), 1)
		
		self.create_setting_ctrl()


	def moduleStructure(self):
		# self.human_preset.fullBodyCtrlRig()
		super(AssetChayaBirdSetup, self)
		# for side in'Lf_', 'Rt_':
		#     self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)

	def post_build(self):
		super(AssetChayaBirdSetup, self)
		if self.RibbonLimb_Rig:
			self.add_ctrl_vis()
		self.add_jnt_vis()
		#self.parent_fingerFk_to_skinJnt()
		#self.set_joint_hierarchy()
		#self.prop_ctrl()

		# if self.rigType == 'deformation':
		#     rt.add_01_attr(self.setting_ctrl.ctrl, 'volumeCtrl')
		#     self.setting_ctrl.ctrl.pynode.attr('volumeCtrl') >> pmc.PyNode(self.vol_jnt).v

	def parent_fingerFk_to_skinJnt(self):
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
			for fin in fingers_list:
				cmds.parent('{}_{}Fin_0_Fk_Jnt'.format(side, fin), '{}_Hand_0_Skin_Jnt'.format(side))
	

	def nonRoll_joint(self):
		for i in 'Lf_', 'Rt_':
			self.nrJnt_leg(i,extra='')
			self.nrJnt_extraLeg(i,extra='')

	def nrJnt_extraLeg(self,side,extra):

		mc.select('%sFeet%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sToe%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legExtraNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='')


	def nrJnt_leg(self,side,extra):
		mc.select('%sUpLeg%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='Hip_0_Anim_Jnt')

	def ribbonCtrl_leg(self, side='l', extra='', skinJnt=True):

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra))
			mc.select('%sUpLeg%s_0_NrTw_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Up', part='UpLeg%s' % extra, skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sLowLeg%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='MidLeg%s' % extra,
														skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%sMid_Rib_Ctl' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			mc.select('%sLowMidLeg%sTop_RibPosi_Loc' % (side, extra), tgl=True)
			mc.select('%sUpUpLeg%sLow_RibPosi_Loc' % (side, extra), tgl=True)
			RigBipedModule.Ribbon.ribbonPointCtl(side=side, part='Leg%s' % extra, shape='sphere')

		for side in 'Lf_', 'Rt_':
			mc.select('%sFeet%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='LowLeg%s' % extra,
														skinJnt=skinJnt)


	def set_joint_hierarchy(self):
		# create finger skin joint
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
		  for fin in fingers_list:
			first_fin = '{}_{}Fin_0_Fk_Jnt'.format(side, fin)
			ch_jnt = cmds.listRelatives(first_fin, ad=1, type = 'joint')
			ch_jnt.append(first_fin)
			skn_tmp = []
			for jnt in ch_jnt[::-1]:
			  if 'Pos' not in jnt:
				skn_tmp.append(jnt)
			skin_jnt_list = []

			for jnt in skn_tmp:
			  skn_jnt= cmds.createNode('joint', n= jnt.replace('_Fk_','_Skin_'))
			  cmds.delete(cmds.parentConstraint(jnt, skn_jnt, mo=0))
			  cmds.makeIdentity(skn_jnt, t=0, r=1, s=1, a=1)
			  skin_jnt_list.append(skn_jnt)

			for num in range(len(skin_jnt_list)-1):
			  cmds.parent(skin_jnt_list[num+1], skin_jnt_list[num])
			
			cmds.parent(skin_jnt_list[0], '{}_Hand_0_Skin_Jnt'.format(side))
			
			for fkJnt, sknJnt in zip(skn_tmp, skin_jnt_list):
			  cmds.parentConstraint(fkJnt, sknJnt,mo=1)
		
		# create reference joint
		ref_jnt = cmds.createNode('joint',n='Reference')
		cmds.parent(ref_jnt,'SkinJnt_Grp')
		cmds.parent('Body_0_Skin_Jnt',ref_jnt)
		
		# parent eye joint to head if eye exists
		for side in 'Lf', 'Rt':
			if mc.objExists('{}_Eye_0_Skin_Jnt'.format(side)):
				cmds.parent('{}_Eye_0_Skin_Jnt'.format(side), 'Head_0_Skin_Jnt')

		# create hand prop joint
		handProp_L =cmds.createNode('joint',n = 'Lf_Prop_0_Skin_Jnt')
		handProp_R = cmds.createNode('joint',n = 'Rt_Prop_0_Skin_Jnt')
		# snap hand joint prop
		cmds.delete(cmds.parentConstraint('Lf_Hand_0_Skin_Jnt', handProp_L, mo=0))
		cmds.delete(cmds.parentConstraint('Rt_Hand_0_Skin_Jnt', handProp_R, mo=0))

		# freeze transform
		cmds.makeIdentity(handProp_L,r=1,s=0,a=1,t=0)
		cmds.makeIdentity(handProp_R,r=1,s=0,a=1,t=0)
		# offset hand prop
		pml = cmds.xform(handProp_L,ws=1,q=1,m=1)
		pmr = cmds.xform(handProp_R,ws=1,q=1,m=1)

		cmds.move(pml[8]*3,pml[9]*3,pml[10]*3,handProp_L,r=1)
		cmds.move(-pmr[8]*3,-pmr[9]*3,-pmr[10]*3,handProp_R,r=1)

		cmds.move(5 ,0 ,0 ,handProp_L,os=1, r=1)
		cmds.move(-5,0,0, handProp_R ,os=1 ,r=1)
		# parent prop to hand
		cmds.parent(handProp_L,'Lf_Hand_0_Skin_Jnt')
		cmds.parent(handProp_R,'Rt_Hand_0_Skin_Jnt')
	
	def prop_ctrl(self):
		mc.addAttr(self.setting_ctrl.ctrl.name,ln='propCtrl',k=1,dv=0,max=1,min=0)
		for side,color in zip(['Lf','Rt'],['red','blue']):
			ctrl = cr.Ctrl(name = 'prop',
							pos = side,
							idx=0,
							des = 'Fk',
							crvShape = 'cube',
							size = 4,
							gimbal=1,
							color = color,
							obj = '{}_Prop_0_Skin_Jnt'.format(side))
			mc.parentConstraint(ctrl.ctrl.name,'{}_Prop_0_Skin_Jnt'.format(side),mo=1)

			rt.add_local_world(ctrl=ctrl.ctrl.name,world_obj='Anim_Grp',local_obj='{}_Hand_0_Skin_Jnt'.format(side),cons_grp=ctrl.zrGrp.name,types='parent')

			mc.parent(ctrl.zrGrp.name,'Anim_Grp')
			mc.connectAttr(self.setting_ctrl.ctrl.name+'.propCtrl',ctrl.zrGrp.name+'.v' )



	def create_setting_ctrl(self):
		self.setting_ctrl = cr.Ctrl(name='setting', crvShape='gear', color='yellow', gimbal=0, size=50, des=N.ANIM)
		self.setting_ctrl.zrGrp.pynode.setParent('Anim_Grp')
		for trans in 'trs':
			for ax in 'xyz':
				self.setting_ctrl.ctrl.pynode.attr('{}{}'.format(trans, ax)).set(k=0, l=1)
		self.setting_ctrl.ctrl.pynode.v.set(k=0, l=1)



	def add_ctrl_vis(self):
		parts = {'Arm': ['Arm', 'Elbow'], 'Leg': ['UpLeg', 'MidLeg','LowLeg']}
		sides = ['Lf', 'Rt']

		rt.add_lock_attr(self.setting_ctrl.ctrl, 'visibilityCtrl')

		# add topic attr
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'ribbonVis')
		# add limb ribbon vis
		for part in parts.keys():
			for side in sides:
				rt.add_01_attr(self.setting_ctrl.ctrl, '{}{}Ribbon'.format(side, part))
				for sub_part in parts[part]:
					rib_ctrl_grp = pmc.PyNode('{}_{}_RibCtl_Grp'.format(side, sub_part))
					self.setting_ctrl.ctrl.pynode.attr('{}{}Ribbon'.format(side, part)) >> rib_ctrl_grp.v

		for part in ['Arm', 'Leg']:
			for side in sides:
				rib_point_grp = pmc.PyNode('{}_{}_RibPoint_CtlGrp'.format(side, part))
				self.setting_ctrl.ctrl.pynode.attr('{}{}Ribbon'.format(side, part)) >> rib_point_grp.v

	def add_jnt_vis(self):
		# add ctrlJoint/skinJoint vis
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'jntVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'ctrlJointVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'skinJointVis')
		self.setting_ctrl.ctrl.pynode.attr('ctrlJointVis') >> pmc.PyNode('CtlJnt_Grp').v
		self.setting_ctrl.ctrl.pynode.attr('skinJointVis') >> pmc.PyNode('SkinJnt_Grp').v


class AssetChayaCockroachSetup(rigAssetPreset.HumanPreset):
	#Eyes_Rig = True
	def __init__(self, *args, **kwargs):
		super(AssetChayaCockroachSetup, self).__init__(*args, **kwargs)

	def initialize(self):
		self.human_preset = rigAssetPreset.rigBipedAsset.BipedRig()
		self.quadruped_preset = rigAssetPreset.rigQuadrupedAsset.QuadrupedRig()
		self.human_preset.rigGrp()
		self.human_preset.placementCtrlRig()
		self.human_preset.bodyCtrlRig()
		self.human_preset.spineCtrlRig()
		self.human_preset.neckCtrlRig(parent=self.human_preset.spineMainJnt_list[-1])
		self.human_preset.headCtrlRig(parent=self.human_preset.neckJnt_list[-1])
		# if self.Eyes_Rig:
		# 	self.human_preset.eyesCtrlRig(parent= self.human_preset.headJnt_list[0])
		#l_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Lf_')
		#r_shoulderSkinJnt_list = self.human_preset.shoulderCtrlRig(parent=self.human_preset.spineMainJnt_list[-1], extra='', side='Rt_')
		#l_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Lf_')
		#r_armSkinJnt_list = self.human_preset.armCtrlRig(parent='Shoulder_0_Anim_Jnt', extra='', side='Rt_')

		# l_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Lf_')
		# r_legSkinJnt_list = self.human_preset.legCtrlRig(parent='Hip_0_Anim_Jnt', extra='', side='Rt_')
		# self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Lf_')
		# self.human_preset.fingerCtrlRig(parent='Hand_0_Anim_Jnt', extra='', side='Rt_')

		# self.human_preset.nrJnt_arm(side='Lf_', extra='')
		# self.human_preset.nrJnt_arm(side='Rt_', extra='')
		# self.human_preset.nrJnt_leg(side='Lf_', extra='')
		# self.human_preset.nrJnt_leg(side='Rt_', extra='')

		# parent MainGrp into placement ctl
		mc.parent(self.human_preset.anim_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ctlJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.skinJnt_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.ikH_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.distance_grp, self.human_preset.placementOffset_ctrl[0])
		mc.parent(self.human_preset.jntTmp_grp, self.human_preset.still_grp)

		# parent spineCtl to Body ctl
		hip_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[0]['oriGrp']
		chest_ctlIkOriGrp = self.human_preset.spineIkCtl_listDict[3]['oriGrp']
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[0], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], self.human_preset.fkCtlOriGrp_list[1], mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], hip_ctlIkOriGrp, mo=True)
		mc.parentConstraint(self.human_preset.bodyCtlDict['ctlGmb'], chest_ctlIkOriGrp, mo=True)

		mc.parent(self.human_preset.spineSkinJnt_list[0], self.human_preset.bodyJntSkin_List[0])
		mc.parent(self.human_preset.neckSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		mc.parent(self.human_preset.headSkinJnt_list[0], self.human_preset.neckSkinJnt_list[-1])
		# mc.parent(self.human_preset.l_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])
		# mc.parent(self.human_preset.r_eyeSkinJntList[0],self.human_preset.headSkinJnt_list[0])

		# mc.parent(l_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		# mc.parent(r_shoulderSkinJnt_list[0], self.human_preset.spineSkinJnt_list[-1])
		# mc.parent(l_armSkinJnt_list[0], l_shoulderSkinJnt_list[-1])
		# mc.parent(r_armSkinJnt_list[0], r_shoulderSkinJnt_list[-1])

		# mc.parent(l_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])
		# mc.parent(r_legSkinJnt_list[0], self.human_preset.spineSkinJnt_list[0])


		#Add quadrupedRig leg module
		for side in 'Lf_', 'Rt_':
			self.quadruped_preset.legCtlRig2(parent='Spine_3_Skin_Jnt', side=side, part='A')
			self.quadruped_preset.legCtlRig2(parent='Spine_1_Skin_Jnt', side=side, part='B')
			self.quadruped_preset.legCtlRig2(parent='Hip_0_Anim_Jnt', side=side, part='C')


		if self.RibbonLimb_Rig:
			self.nonRoll_joint()
			for part in 'ABC':
				self.ribbonCtrl_leg(extra= part)
			for side in 'Lf_','Rt_':
				for part in 'ABC':
					#self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)
					cmds.setAttr('{}UpLeg{}Mid_Rib_Ctl.autoTwist'.format(side,part), 1)
					cmds.setAttr('{}MidLeg{}Mid_Rib_Ctl.autoTwist'.format(side,part), 1)
					cmds.setAttr('{}LowLeg{}Mid_Rib_Ctl.autoTwist'.format(side,part), 1)
					# cmds.setAttr('{}ElbowMid_Rib_Ctl.autoTwist'.format(side), 1)
					# cmds.setAttr('{}ArmMid_Rib_Ctl.autoTwist'.format(side), 1)
			
		self.create_setting_ctrl()


	def moduleStructure(self):
		# self.human_preset.fullBodyCtrlRig()
		super(AssetChayaCockroachSetup, self)
		# for side in'Lf_', 'Rt_':
		#     self.human_preset.ribbonCtrl_arm(side=side, extra='', skinJnt=True)

	def post_build(self):
		super(AssetChayaCockroachSetup, self)
		if self.RibbonLimb_Rig:
			self.add_ctrl_vis()
		self.add_jnt_vis()
		#self.parent_fingerFk_to_skinJnt()
		#self.set_joint_hierarchy()
		#self.prop_ctrl()

		# if self.rigType == 'deformation':
		#     rt.add_01_attr(self.setting_ctrl.ctrl, 'volumeCtrl')
		#     self.setting_ctrl.ctrl.pynode.attr('volumeCtrl') >> pmc.PyNode(self.vol_jnt).v

	def parent_fingerFk_to_skinJnt(self):
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
			for fin in fingers_list:
				cmds.parent('{}_{}Fin_0_Fk_Jnt'.format(side, fin), '{}_Hand_0_Skin_Jnt'.format(side))
	

	def nonRoll_joint(self):
		for i in 'Lf_', 'Rt_':
			for part in 'ABC':
				self.nrJnt_leg(i,extra=part)
				self.nrJnt_extraLeg(i,extra=part)

	def nrJnt_extraLeg(self,side,extra):

		mc.select('%sFeet%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sToe%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legExtraNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='')


	def nrJnt_leg(self,side,extra):
		mc.select('%sUpLeg%s_0_Anim_Jnt' % (side, extra))
		mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
		legNrJnt_rig = RigBipedModule.nonrollJnt.nrJnt(side=side, parent='Hip_0_Anim_Jnt')

	def ribbonCtrl_leg(self, side='l', extra='', skinJnt=True):

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra))
			mc.select('%sUpLeg%s_0_NrTw_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Up', part='UpLeg%s' % extra, skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sLowLeg%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='MidLeg%s' % extra,
														skinJnt=skinJnt)

		for side in 'Lf_', 'Rt_':
			mc.select('%sMidLeg%sMid_Rib_Ctl' % (side, extra))
			mc.select('%sMidLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			mc.select('%sLowMidLeg%sTop_RibPosi_Loc' % (side, extra), tgl=True)
			mc.select('%sUpUpLeg%sLow_RibPosi_Loc' % (side, extra), tgl=True)
			RigBipedModule.Ribbon.ribbonPointCtl(side=side, part='Leg%s' % extra, shape='sphere')

		for side in 'Lf_', 'Rt_':
			mc.select('%sFeet%s_0_NrTw_Jnt' % (side, extra))
			mc.select('%sLowLeg%s_0_Anim_Jnt' % (side, extra), tgl=True)
			upArmRib_rig = RigBipedModule.Ribbon.ribbon(side=side, twist='Low', part='LowLeg%s' % extra,
														skinJnt=skinJnt)


	def set_joint_hierarchy(self):
		# create finger skin joint
		fingers_list = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
		for side in 'Lf', 'Rt':
		  for fin in fingers_list:
			first_fin = '{}_{}Fin_0_Fk_Jnt'.format(side, fin)
			ch_jnt = cmds.listRelatives(first_fin, ad=1, type = 'joint')
			ch_jnt.append(first_fin)
			skn_tmp = []
			for jnt in ch_jnt[::-1]:
			  if 'Pos' not in jnt:
				skn_tmp.append(jnt)
			skin_jnt_list = []

			for jnt in skn_tmp:
			  skn_jnt= cmds.createNode('joint', n= jnt.replace('_Fk_','_Skin_'))
			  cmds.delete(cmds.parentConstraint(jnt, skn_jnt, mo=0))
			  cmds.makeIdentity(skn_jnt, t=0, r=1, s=1, a=1)
			  skin_jnt_list.append(skn_jnt)

			for num in range(len(skin_jnt_list)-1):
			  cmds.parent(skin_jnt_list[num+1], skin_jnt_list[num])
			
			cmds.parent(skin_jnt_list[0], '{}_Hand_0_Skin_Jnt'.format(side))
			
			for fkJnt, sknJnt in zip(skn_tmp, skin_jnt_list):
			  cmds.parentConstraint(fkJnt, sknJnt,mo=1)
		
		# create reference joint
		ref_jnt = cmds.createNode('joint',n='Reference')
		cmds.parent(ref_jnt,'SkinJnt_Grp')
		cmds.parent('Body_0_Skin_Jnt',ref_jnt)
		
		# parent eye joint to head if eye exists
		for side in 'Lf', 'Rt':
			if mc.objExists('{}_Eye_0_Skin_Jnt'.format(side)):
				cmds.parent('{}_Eye_0_Skin_Jnt'.format(side), 'Head_0_Skin_Jnt')

		# create hand prop joint
		handProp_L =cmds.createNode('joint',n = 'Lf_Prop_0_Skin_Jnt')
		handProp_R = cmds.createNode('joint',n = 'Rt_Prop_0_Skin_Jnt')
		# snap hand joint prop
		cmds.delete(cmds.parentConstraint('Lf_Hand_0_Skin_Jnt', handProp_L, mo=0))
		cmds.delete(cmds.parentConstraint('Rt_Hand_0_Skin_Jnt', handProp_R, mo=0))

		# freeze transform
		cmds.makeIdentity(handProp_L,r=1,s=0,a=1,t=0)
		cmds.makeIdentity(handProp_R,r=1,s=0,a=1,t=0)
		# offset hand prop
		pml = cmds.xform(handProp_L,ws=1,q=1,m=1)
		pmr = cmds.xform(handProp_R,ws=1,q=1,m=1)

		cmds.move(pml[8]*3,pml[9]*3,pml[10]*3,handProp_L,r=1)
		cmds.move(-pmr[8]*3,-pmr[9]*3,-pmr[10]*3,handProp_R,r=1)

		cmds.move(5 ,0 ,0 ,handProp_L,os=1, r=1)
		cmds.move(-5,0,0, handProp_R ,os=1 ,r=1)
		# parent prop to hand
		cmds.parent(handProp_L,'Lf_Hand_0_Skin_Jnt')
		cmds.parent(handProp_R,'Rt_Hand_0_Skin_Jnt')
	
	def prop_ctrl(self):
		mc.addAttr(self.setting_ctrl.ctrl.name,ln='propCtrl',k=1,dv=0,max=1,min=0)
		for side,color in zip(['Lf','Rt'],['red','blue']):
			ctrl = cr.Ctrl(name = 'prop',
							pos = side,
							idx=0,
							des = 'Fk',
							crvShape = 'cube',
							size = 4,
							gimbal=1,
							color = color,
							obj = '{}_Prop_0_Skin_Jnt'.format(side))
			mc.parentConstraint(ctrl.ctrl.name,'{}_Prop_0_Skin_Jnt'.format(side),mo=1)

			rt.add_local_world(ctrl=ctrl.ctrl.name,world_obj='Anim_Grp',local_obj='{}_Hand_0_Skin_Jnt'.format(side),cons_grp=ctrl.zrGrp.name,types='parent')

			mc.parent(ctrl.zrGrp.name,'Anim_Grp')
			mc.connectAttr(self.setting_ctrl.ctrl.name+'.propCtrl',ctrl.zrGrp.name+'.v' )



	def create_setting_ctrl(self):
		self.setting_ctrl = cr.Ctrl(name='setting', crvShape='gear', color='yellow', gimbal=0, size=50, des=N.ANIM)
		self.setting_ctrl.zrGrp.pynode.setParent('Anim_Grp')
		for trans in 'trs':
			for ax in 'xyz':
				self.setting_ctrl.ctrl.pynode.attr('{}{}'.format(trans, ax)).set(k=0, l=1)
		self.setting_ctrl.ctrl.pynode.v.set(k=0, l=1)



	def add_ctrl_vis(self):
		parts = {'Leg': ['UpLeg', 'MidLeg','LowLeg']}
		sides = ['Lf', 'Rt']

		rt.add_lock_attr(self.setting_ctrl.ctrl, 'visibilityCtrl')

		# add topic attr
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'ribbonVis')
		# add limb ribbon vis
		for part in parts.keys():
			for elem in 'ABC':
				for side in sides:
					rt.add_01_attr(self.setting_ctrl.ctrl, '{}{}{}Ribbon'.format(side, part,elem))
					for sub_part in parts[part]:
						rib_ctrl_grp = pmc.PyNode('{}_{}{}_RibCtl_Grp'.format(side, sub_part,elem))
						self.setting_ctrl.ctrl.pynode.attr('{}{}{}Ribbon'.format(side, part,elem)) >> rib_ctrl_grp.v

		for part in ['Leg']:
			for side in sides:
				for elem in 'ABC':
					rib_point_grp = pmc.PyNode('{}_{}{}_RibPoint_CtlGrp'.format(side, part,elem))
					self.setting_ctrl.ctrl.pynode.attr('{}{}{}Ribbon'.format(side, part,elem)) >> rib_point_grp.v

	def add_jnt_vis(self):
		# add ctrlJoint/skinJoint vis
		rt.add_lock_attr(self.setting_ctrl.ctrl, 'jntVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'ctrlJointVis')
		rt.add_01_attr(self.setting_ctrl.ctrl, 'skinJointVis')
		self.setting_ctrl.ctrl.pynode.attr('ctrlJointVis') >> pmc.PyNode('CtlJnt_Grp').v
		self.setting_ctrl.ctrl.pynode.attr('skinJointVis') >> pmc.PyNode('SkinJnt_Grp').v
